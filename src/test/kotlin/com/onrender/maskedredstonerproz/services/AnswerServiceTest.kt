package com.onrender.maskedredstonerproz.services

import com.google.common.truth.Truth.assertThat
import com.onrender.maskedredstonerproz.data.models.Answer
import com.onrender.maskedredstonerproz.data.models.exceptions.answer.fill_blanks.AnswerEmptyBlankPositionsListException
import com.onrender.maskedredstonerproz.data.models.exceptions.answer.fill_blanks.AnswerEmptySentencesListException
import com.onrender.maskedredstonerproz.data.models.exceptions.answer.match_descriptions.AnswerDescriptionsListHasEmptyStringException
import com.onrender.maskedredstonerproz.data.models.exceptions.answer.match_descriptions.AnswerEmptyDescriptionsListException
import com.onrender.maskedredstonerproz.data.models.exceptions.answer.match_descriptions.AnswerEmptyPhrasesListException
import com.onrender.maskedredstonerproz.data.models.exceptions.answer.match_descriptions.AnswerPhrasesListHasEmptyStringException
import com.onrender.maskedredstonerproz.data.models.exceptions.answer.multiple_choice.AnswerChoiceListItemUnderflowException
import com.onrender.maskedredstonerproz.data.models.exceptions.answer.multiple_choice.AnswerCorrectAnswerChoiceListHasEmptyStringException
import com.onrender.maskedredstonerproz.data.models.exceptions.answer.multiple_choice.AnswerCorrectAnswerChoiceListItemUnderflowException
import com.onrender.maskedredstonerproz.data.models.exceptions.answer.multiple_choice.AnswerEmptyCorrectAnswerChoiceListException
import com.onrender.maskedredstonerproz.data.models.exceptions.answer.sequencing.AnswerEmptyTextListException
import com.onrender.maskedredstonerproz.data.models.exceptions.answer.sequencing.AnswerTextListHasEmptyStringException
import com.onrender.maskedredstonerproz.data.models.exceptions.answer.single_choice.AnswerChoiceListHasEmptyStringException
import com.onrender.maskedredstonerproz.data.models.exceptions.answer.single_choice.AnswerChoiceListHasOnlyOneItemException
import com.onrender.maskedredstonerproz.data.models.exceptions.answer.single_choice.AnswerCorrectAnswerChoiceEmptyStringException
import com.onrender.maskedredstonerproz.data.models.exceptions.answer.single_choice.AnswerEmptyChoiceListException
import com.onrender.maskedredstonerproz.data.repos.FakeAnswerRepository
import com.onrender.maskedredstonerproz.util.makePositionsOfBlanks
import io.ktor.server.testing.*
import org.junit.Assert.assertThrows
import org.junit.Before
import org.junit.Test
import com.onrender.maskedredstonerproz.data.models.exceptions.answer.multiple_choice.AnswerChoiceListHasEmptyStringException as AnswerMultipleChoiceChoiceListHasEmptyStringException
import com.onrender.maskedredstonerproz.data.models.exceptions.answer.multiple_choice.AnswerEmptyChoiceListException as AnswerEmptyMultipleChoiceChoiceListException

class AnswerServiceTest {

    private lateinit var repository: FakeAnswerRepository
    private lateinit var service: AnswerService

    @Before
    fun setUp() {
        repository = FakeAnswerRepository()
        service = AnswerService(repository)
    }

    @Test
    fun `create single choice answer with valid data, success`() = testApplication {

        val answer: Answer = Answer.SingleChoice(
            choices = listOf(
                "kolovozna traka",
                "saobracajna traka",
                "kolovoz",
            ),
            correctAnswerChoice = "kolovozna traka"
        )

        service.createAnswer(answer)

        assertThat(repository.answers).contains(answer)
    }

    @Test
    fun `create single choice answer with empty choice list, not created, AnswerEmptyChoiceListException is thrown`() {

        val answer: Answer = Answer.SingleChoice(
            choices = emptyList(),
            correctAnswerChoice = "kolovozna traka"
        )

        assertThrows(AnswerEmptyChoiceListException::class.java) {
            testApplication {
                service.createAnswer(answer)
            }
        }
    }

    @Test
    fun `create single choice answer with choice list that has an empty string, not created, AnswerChoiceListHasEmptyString exception is thrown`() =
        testApplication {

            val answer: Answer = Answer.SingleChoice(
                choices = listOf(
                    "kolovozna traka",
                    "",
                    "kolovoz",
                ),
                correctAnswerChoice = "kolovozna traka"
            )

            assertThrows(AnswerChoiceListHasEmptyStringException::class.java) {
                testApplication {
                    service.createAnswer(answer)
                }
            }
        }

    @Test
    fun `create single choice answer with correct choice that is an empty string, not created, AnswerCorrectChoiceEmptyString exception is thrown`() =
        testApplication {

            val answer: Answer = Answer.SingleChoice(
                choices = listOf(
                    "kolovozna traka",
                    "saobracajna traka",
                    "kolovoz",
                ),
                correctAnswerChoice = ""
            )

            assertThrows(AnswerCorrectAnswerChoiceEmptyStringException::class.java) {
                testApplication {
                    service.createAnswer(answer)
                }
            }
        }

    @Test
    fun `create single choice answer with choice list that has only 1 item, not created, AnswerChoiceListContainsEmptyString exception is thrown`() =
        testApplication {

            val answer: Answer = Answer.SingleChoice(
                choices = listOf(
                    "kolovozna traka"
                ),
                correctAnswerChoice = "kolovozna traka"
            )

            assertThrows(AnswerChoiceListHasOnlyOneItemException::class.java) {
                testApplication {
                    service.createAnswer(answer)
                }
            }
        }

    @Test
    fun `create true-false answer with valid data, success`() = testApplication {

        val answer: Answer = Answer.TrueFalse(
            true
        )

        service.createAnswer(answer)

        assertThat(repository.answers).contains(answer)
    }

    @Test
    fun `create sequencing answer with valid data, success`() = testApplication {

        val answer: Answer = Answer.Sequencing(
            listOf(
                "Interpersonalna komunikacija",
                "Grupna komunikacija",
                "Javna komunikacija",
                "Masovna komunikacija",
                "Organizaciona komunikacija",
                "Intrapersonalna komunikacija",
                "Medjukulturalna komunikacija"
            )
        )

        service.createAnswer(answer)

        assertThat(repository.answers).contains(answer)
    }

    @Test
    fun `create sequencing answer with empty answer text list, not created, AnswerEmptyTextList exception is thrown`() =
        testApplication {

            val answer: Answer = Answer.Sequencing(
                emptyList()
            )

            assertThrows(AnswerEmptyTextListException::class.java) {
                testApplication {
                    service.createAnswer(answer)
                }
            }
        }

    @Test
    fun `create sequencing answer with answer text list that has an empty string, not created, AnswerTextListHasEmptyString exception is thrown`() =
        testApplication {

            val answer: Answer = Answer.Sequencing(
                listOf(
                    "Interpersonalna komunikacija",
                    "Grupna komunikacija",
                    "Javna komunikacija",
                    "",
                    "Organizaciona komunikacija",
                    "Intrapersonalna komunikacija",
                    "Medjukulturalna komunikacija"
                )
            )

            assertThrows(AnswerTextListHasEmptyStringException::class.java) {
                testApplication {
                    service.createAnswer(answer)
                }
            }
        }

    @Test
    fun `create matching descriptions answer with valid data, success`() = testApplication {

        val answer: Answer = Answer.MatchDescriptions(
            phrases = listOf(
                "Intimna zona",
                "Licna zona",
                "Socialna zona",
                "Javna zona"
            ),
            descriptions = listOf(
                "do 45 cm",
                "od 45 cm do 1.2 m",
                "od 1,2 m do 3 m",
                "vise od 3 m"
            )
        )

        service.createAnswer(answer)

        assertThat(repository.answers).contains(answer)
    }

    @Test
    fun `create matching descriptions answer with empty phrases list, not created, AnswerEmptyPhrasesList exception is thrown`() =
        testApplication {

            val answer: Answer = Answer.MatchDescriptions(
                phrases = emptyList(),
                descriptions = listOf(
                    "do 45 cm",
                    "od 45 cm do 1.2 m",
                    "od 1,2 m do 3 m",
                    "vise od 3 m"
                )
            )

            assertThrows(AnswerEmptyPhrasesListException::class.java) {
                testApplication {
                    service.createAnswer(answer)
                }
            }
        }

    @Test
    fun `create matching descriptions answer with empty descriptions list, not created, AnswerEmptyDescriptionsList exception is thrown`() =
        testApplication {

            val answer: Answer = Answer.MatchDescriptions(
                phrases = listOf(
                    "Intimna zona",
                    "Licna zona",
                    "Socialna zona",
                    "Javna zona"
                ),
                descriptions = emptyList()
            )

            assertThrows(AnswerEmptyDescriptionsListException::class.java) {
                testApplication {
                    service.createAnswer(answer)
                }
            }
        }

    @Test
    fun `create matching descriptions answer with phrases list that has an empty string, not created, AnswerPhrasesListHasEmptyString exception is thrown`() =
        testApplication {

            val answer: Answer = Answer.MatchDescriptions(
                phrases = listOf(
                    "Intimna zona",
                    "",
                    "Socialna zona",
                    "Javna zona"
                ),
                descriptions = listOf(
                    "do 45 cm",
                    "od 45 cm do 1,2 m",
                    "od 1,2 m do 3 m",
                    "vise od 3 m"
                )
            )

            assertThrows(AnswerPhrasesListHasEmptyStringException::class.java) {
                testApplication {
                    service.createAnswer(answer)
                }
            }
        }

    @Test
    fun `create matching descriptions answer with descriptions list that has an empty string, not created, AnswerDescriptionsListHasEmptyString exception is thrown`() =
        testApplication {

            val answer: Answer = Answer.MatchDescriptions(
                phrases = listOf(
                    "Intimna zona",
                    "Licna zona",
                    "Socialna zona",
                    "Javna zona"
                ),
                descriptions = listOf(
                    "do 45 cm",
                    "",
                    "od 1,2 m do 3 m",
                    "vise od 3 m"
                )
            )

            assertThrows(AnswerDescriptionsListHasEmptyStringException::class.java) {
                testApplication {
                    service.createAnswer(answer)
                }
            }
        }

    @Test
    fun `create multiple choice answer with valid data, success`() = testApplication {

        val answer: Answer = Answer.MultipleChoice(
            choices = listOf(
                "na putu sa mokrim kolovozom",
                "koje nije obiljezenno posebnim znakom",
                "na autoputu brzinom vecom od 120 km/h",
                "na brzom putu brzinom vecom od 90 km/h"
            ),
            correctAnswerChoices = listOf(
                "koje nije obiljezenno posebnim znakom",
                "na autoputu brzinom vecom od 120 km/h",
                "na brzom putu brzinom vecom od 90 km/h"
            )
        )

        service.createAnswer(answer)

        assertThat(repository.answers).contains(answer)
    }

    @Test
    fun `create multiple choice answer with empty choices list, not created, AnswerEmptyChoiceList exception is thrown`() =
        testApplication {

            val answer: Answer = Answer.MultipleChoice(
                choices = emptyList(),
                correctAnswerChoices = listOf(
                    "koje nije obiljezenno posebnim znakom",
                    "na autoputu brzinom vecom od 120 km/h",
                    "na brzom putu brzinom vecom od 90 km/h"
                )
            )

            assertThrows(AnswerEmptyMultipleChoiceChoiceListException::class.java) {
                testApplication {
                    service.createAnswer(answer)
                }
            }
        }

    @Test
    fun `create multiple choice answer with empty correct answer choices list, not created, AnswerEmptyCorrectChoiceList exception is thrown`() =
        testApplication {

            val answer: Answer = Answer.MultipleChoice(
                choices = listOf(
                    "na putu sa mokrim kolovozom",
                    "koje nije obiljezenno posebnim znakom",
                    "na autoputu brzinom vecom od 120 km/h",
                    "na brzom putu brzinom vecom od 90 km/h"
                ),
                correctAnswerChoices = emptyList()
            )

            assertThrows(AnswerEmptyCorrectAnswerChoiceListException::class.java) {
                testApplication {
                    service.createAnswer(answer)
                }
            }
        }

    @Test
    fun `create multiple choice answer with choices list that has an empty string, not created, AnswerChoiceListHasEmptyString exception is thrown`() =
        testApplication {

            val answer: Answer = Answer.MultipleChoice(
                choices = listOf(
                    "na putu sa mokrim kolovozom",
                    "",
                    "na autoputu brzinom vecom od 120 km/h",
                    "na brzom putu brzinom vecom od 90 km/h"
                ),
                correctAnswerChoices = listOf(
                    "koje nije obiljezenno posebnim znakom",
                    "na autoputu brzinom vecom od 120 km/h",
                    "na brzom putu brzinom vecom od 90 km/h"
                )
            )

            assertThrows(AnswerMultipleChoiceChoiceListHasEmptyStringException::class.java) {
                testApplication {
                    service.createAnswer(answer)
                }
            }
        }

    @Test
    fun `create multiple choice answer with correct answer choices list that has an empty string, not created, AnswerCorrectAnswerChoiceListHasEmptyString exception is thrown`() =
        testApplication {

            val answer: Answer = Answer.MultipleChoice(
                choices = listOf(
                    "na putu sa mokrim kolovozom",
                    "koje nije obiljezenno posebnim znakom",
                    "na autoputu brzinom vecom od 120 km/h",
                    "na brzom putu brzinom vecom od 90 km/h"
                ),
                correctAnswerChoices = listOf(
                    "koje nije obiljezenno posebnim znakom",
                    "",
                    "na brzom putu brzinom vecom od 90 km/h"
                )
            )

            assertThrows(AnswerCorrectAnswerChoiceListHasEmptyStringException::class.java) {
                testApplication {
                    service.createAnswer(answer)
                }
            }
        }

    @Test
    fun `create multiple choice answer with choices list that has two items, not created, AnswerChoiceListItemUnderflow exception is thrown`() =
        testApplication {

            val answer: Answer = Answer.MultipleChoice(
                choices = listOf(
                    "na putu sa mokrim kolovozom",
                    "koje nije obiljezenno posebnim znakom",
                ),
                correctAnswerChoices = listOf(
                    "koje nije obiljezenno posebnim znakom",
                )
            )

            assertThrows(AnswerChoiceListItemUnderflowException::class.java) {
                testApplication {
                    service.createAnswer(answer)
                }
            }
        }

    @Test
    fun `create multiple choice answer with correct answer choices list that has one item, not created, AnswerCorrectAnswerChoiceListItemUnderflow exception is thrown`() =
        testApplication {

            val answer: Answer = Answer.MultipleChoice(
                choices = listOf(
                    "na putu sa mokrim kolovozom",
                    "koje nije obiljezenno posebnim znakom",
                    "na autoputu brzinom vecom od 120 km/h",
                    "na brzom putu brzinom vecom od 90 km/h"
                ),
                correctAnswerChoices = listOf(
                    "koje nije obiljezenno posebnim znakom",
                )
            )

            assertThrows(AnswerCorrectAnswerChoiceListItemUnderflowException::class.java) {
                testApplication {
                    service.createAnswer(answer)
                }
            }
        }

    @Test
    fun `create fill blanks answer with valid data, success`() = testApplication {

        val sentences = listOf(
            "The cat chased the butterfly through the garden",
            "She decided to read on a rainy day",
            "The vibrant colours of the sunset painted the sky",
            "The birds sang a sweet melody in the morning"
        )

        val answer: Answer = Answer.FillTheBlanks(
            sentences = sentences,
            positionsOfBlanks = sentences.makePositionsOfBlanks(
                "butterfly",
                "read",
                "vibrant",
                "melody"
            )
        )

        service.createAnswer(answer)

        assertThat(repository.answers).contains(answer)

    }

    @Test
    fun `create fill blanks answer with empty sentences list, not created, AnswerEmptySentencesList exception is thrown`() = testApplication {

        val sentences = emptyList<String>()

        val answer: Answer = Answer.FillTheBlanks(
            sentences = sentences,
            positionsOfBlanks = sentences.makePositionsOfBlanks(
                "butterfly",
                "read",
                "vibrant",
                "melody"
            )
        )

        assertThrows(AnswerEmptySentencesListException::class.java) {
            testApplication {
                service.createAnswer(answer)
            }
        }
    }

    @Test
    fun `create fill blanks answer with empty positionsOfBlanks list, not created, AnswerEmptyBlankPositionsList exception is thrown`() = testApplication {

        val sentences = listOf(
            "The cat chased the butterfly through the garden",
            "She decided to read on a rainy day",
            "The vibrant colours of the sunset painted the sky",
            "The birds sang a sweet melody in the morning"
        )

        val answer: Answer = Answer.FillTheBlanks(
            sentences = sentences,
            positionsOfBlanks = emptyList()
        )

        assertThrows(AnswerEmptyBlankPositionsListException::class.java) {
            testApplication {
                service.createAnswer(answer)
            }
        }
    }

}