package com.onrender.maskedredstonerproz.services

import com.google.common.truth.Truth.assertThat
import com.onrender.maskedredstonerproz.data.models.Quiz
import com.onrender.maskedredstonerproz.data.models.exceptions.quiz.QuizEmptyQuestionIdsListException
import com.onrender.maskedredstonerproz.data.models.exceptions.quiz.QuizQuestionIdsListHasEmptyStringException
import com.onrender.maskedredstonerproz.data.models.exceptions.quiz.QuizTitleEmptyStringException
import com.onrender.maskedredstonerproz.data.repos.FakeQuizRepository
import io.ktor.server.testing.*
import org.junit.Assert.assertThrows
import org.junit.Before
import org.junit.Test

class QuizServiceTest {

    private lateinit var repository: FakeQuizRepository
    private lateinit var service: QuizService

    @Before
    fun setUp() {
        repository = FakeQuizRepository()
        service = QuizService(repository)
    }

    @Test
    fun `create quiz with valid data, success`() = testApplication {

        val quiz = Quiz(
            title = "End of semester test",
            description = "The test which students are supposed to do at the end of the semester",
            questionIds = listOf(
                "654ab7da93b9af7fb2c02c32"
            )
        )

        service.createQuiz(quiz)

        assertThat(repository.quizzes).contains(quiz)
    }

    @Test
    fun `create quiz with title that is an empty string, not created, QuizTitleEmptyString exception is thrown`() = testApplication {

        val quiz = Quiz(
            title = "",
            description = "The test which students are supposed to do at the end of the semester",
            questionIds = listOf(
                "654ab7da93b9af7fb2c02c32"
            )
        )

        assertThrows(QuizTitleEmptyStringException::class.java) {
            testApplication {
                service.createQuiz(quiz)
            }
        }

    }

    @Test
    fun `create quiz with empty questionId list, not created, QuizEmptyQuestionIdsList exception is thrown`() = testApplication {

        val quiz = Quiz(
            title = "End of semester test",
            description = "The test which students are supposed to do at the end of the semester",
            questionIds = emptyList()
        )

        assertThrows(QuizEmptyQuestionIdsListException::class.java) {
            testApplication {
                service.createQuiz(quiz)
            }
        }

    }

    @Test
    fun `create quiz with questionIds list that has an empty string, not created, QuizQuestionIdsListHasEmptyString exception is thrown`() = testApplication {

        val quiz = Quiz(
            title = "End of semester test",
            description = "The test which students are supposed to do at the end of the semester",
            questionIds = listOf(
                ""
            )
        )

        assertThrows(QuizQuestionIdsListHasEmptyStringException::class.java) {
            testApplication {
                service.createQuiz(quiz)
            }
        }

    }
}