package com.onrender.maskedredstonerproz.services

import com.google.common.truth.Truth.assertThat
import com.onrender.maskedredstonerproz.data.models.User
import com.onrender.maskedredstonerproz.data.models.exceptions.user.UserEmptyCreatedQuizzesListException
import com.onrender.maskedredstonerproz.data.models.exceptions.user.*
import com.onrender.maskedredstonerproz.data.repos.FakeUserRepository
import com.onrender.maskedredstonerproz.util.UserRole
import io.ktor.server.testing.*
import org.junit.Assert.assertThrows
import org.junit.Before
import org.junit.Test
import org.mindrot.jbcrypt.BCrypt

class UserServiceTest {

    private lateinit var repository: FakeUserRepository
    private lateinit var service: UserService

    @Before
    fun setUp() {
        repository = FakeUserRepository()
        service = UserService(repository)
    }

    @Test
    fun `signs up the user with valid credentials, user is signed up`() = testApplication {
        val user = User(
            name = "MaskedRedstonerProZ",
            role = UserRole.PROFESSOR,
            email = "maskedredstonerproz@gmail.com",
            password = "c3%>MW;P7~gqX_B}?@AwyG"
        )

        service.signUp(user)

        assertThat(repository.users.find { it.email == user.email }).isNotNull()
    }

    @Test
    fun `signs up the user with blank username, user not signed up + UserBlankName exception is thrown`() {
        val user = User(
            name = "",
            role = UserRole.PROFESSOR,
            email = "maskedredstonerproz@gmail.com",
            password = "c3%>MW;P7~gqX_B}?@AwyG"
        )

        assertThrows(UserBlankNameException::class.java) {
            testApplication {
                service.signUp(user)
            }
        }
    }

    @Test
    fun `signs up the user with invalid role, user not signed up + UserInvalidRole exception is thrown`() {
        val user = User(
            name = "MaskedRedstonerProZ",
            role = 7,
            email = "maskedredstonerproz@gmail.com",
            password = "c3%>MW;P7~gqX_B}?@AwyG"
        )

        assertThrows(UserInvalidRoleException::class.java) {
            testApplication {
                service.signUp(user)
            }
        }
    }

    @Test
    fun `signs up the user with blank email, user not signed up + UserBlankEmail exception is thrown`() {
        val user = User(
            name = "MaskedRedstonerProZ",
            role = UserRole.PROFESSOR,
            email = "",
            password = "c3%>MW;P7~gqX_B}?@AwyG"
        )

        assertThrows(UserBlankEmailException::class.java) {
            testApplication {
                service.signUp(user)
            }
        }
    }

    @Test
    fun `signs up the user with invalid email, user not signed up + UserInvalidEmail exception is thrown`() {
        val user = User(
            name = "MaskedRedstonerProZ",
            role = UserRole.PROFESSOR,
            email = "maskedredstonerproz.com",
            password = "c3%>MW;P7~gqX_B}?@AwyG"
        )

        assertThrows(UserInvalidEmailException::class.java) {
            testApplication {
                service.signUp(user)
            }
        }
    }

    @Test
    fun `signs up the user with blank password, user not signed up + UserPasswordLengthUnderflow exception is thrown`() {
        val user = User(
            name = "MaskedRedstonerProZ",
            role = UserRole.PROFESSOR,
            email = "maskedredstonerproz@gmail.com",
            password = ""
        )

        assertThrows(UserPasswordLengthUnderflowException::class.java) {
            testApplication {
                service.signUp(user)
            }
        }
    }

    @Test
    fun `signs up the user with password that lacks a capital letter, user not signed up + UserPasswordLacksCapitalLetter exception is thrown`() {
        val user = User(
            name = "MaskedRedstonerProZ",
            role = UserRole.PROFESSOR,
            email = "maskedredstonerproz@gmail.com",
            password = "c3%>m;p7~qx_b}?@awyg"
        )

        assertThrows(UserPasswordLacksCapitalLetterException::class.java) {
            testApplication {
                service.signUp(user)
            }
        }
    }

    @Test
    fun `signs up the user with password that lacks a number, user not signed up + UserPasswordLacksNumber exception is thrown`() {
        val user = User(
            name = "MaskedRedstonerProZ",
            role = UserRole.PROFESSOR,
            email = "maskedredstonerproz@gmail.com",
            password = "c%>MW;P~gqX_B}?@AwyG"
        )

        assertThrows(UserPasswordLacksNumberException::class.java) {
            testApplication {
                service.signUp(user)
            }
        }
    }

    @Test
    fun `signs up the user with password that lacks a special symbol, user not signed up + UserPasswordLacksSpecialSymbol exception is thrown`() {
        val user = User(
            name = "MaskedRedstonerProZ",
            role = UserRole.PROFESSOR,
            email = "maskedredstonerproz@gmail.com",
            password = "c3MWP7gqXBAwyGfb"
        )

        assertThrows(UserPasswordLacksSpecialSymbolException::class.java) {
            testApplication {
                service.signUp(user)
            }
        }
    }

    @Test
    fun `signs up the user with password that has repeated characters, user not signed up + UserPasswordHasRepeatedCharacters exception is thrown`() {
        val user = User(
            name = "MaskedRedstonerProZ",
            role = UserRole.PROFESSOR,
            email = "maskedredstonerproz@gmail.com",
            password = "c3%>MW;P7~gqX_B}?@AAAwyG"
        )

        assertThrows(UserPasswordHasRepeatedCharactersException::class.java) {
            testApplication {
                service.signUp(user)
            }
        }
    }

    @Test
    fun `signs up the user with password that has sequences such as 123 or abc, user not signed up + UserPasswordHasSequences exception is thrown`() {
        val user = User(
            name = "MaskedRedstonerProZ",
            role = UserRole.PROFESSOR,
            email = "maskedredstonerproz@gmail.com",
            password = "c123%>MW;Pabc~gqX_B}?@AwyG"
        )

        assertThrows(UserPasswordHasSequencesException::class.java) {
            testApplication {
                service.signUp(user)
            }
        }
    }

    @Test
    fun `checks if a user with an email different from the signed up user's email exists, user doesn't exist`() = testApplication {
        val user = User(
            name = "MaskedRedstonerProZ",
            role = UserRole.PROFESSOR,
            email = "maskedredstonerproz@gmail.com",
            password = "c3%>MW;P7~gqX_B}?@AwyG"
        )

        service.signUp(user)

        assertThat(service.doesUserWithEmailExist("test@test.com")).isFalse()
    }

    @Test
    fun `checks if a user with an email the same as the signed up user's email, user does exist`() = testApplication {
        val user = User(
            name = "MaskedRedstonerProZ",
            role = UserRole.PROFESSOR,
            email = "maskedredstonerproz@gmail.com",
            password = "c3%>MW;P7~gqX_B}?@AwyG"
        )

        service.signUp(user)

        assertThat(service.doesUserWithEmailExist(user.email)).isTrue()
    }

    @Test
    fun `tries to get the user by the email, success`() = testApplication {
        val user = User(
            name = "MaskedRedstonerProZ",
            role = UserRole.PROFESSOR,
            email = "maskedredstonerproz@gmail.com",
            password = "c3%>MW;P7~gqX_B}?@AwyG"
        )

        service.signUp(user)

        assertThat(service.getUserByEmail("maskedredstonerproz@gmail.com")).isNotNull()
    }

    @Test
    fun `checks if two equal passwords are validated as such, success`() = testApplication {
        assertThat(service.isPasswordValid("c3%>MW;P7~gqX_B}?@AwyG", BCrypt.hashpw("c3%>MW;P7~gqX_B}?@AwyG", BCrypt.gensalt(5)))).isTrue()
    }

    @Test
    fun `checks if two different passwords are validated as such, success`() = testApplication {
        assertThat(service.isPasswordValid("c3%>MW;P7~gqX_B}?@AwyGgdfswhdfgjdfgh", BCrypt.hashpw("c3%>MW;P7~gqX_B}?@AwyG", BCrypt.gensalt(5)))).isFalse()
    }

    @Test
    fun `update user with createdQuizzes list, success`() = testApplication {

        val user = User(
            name = "MaskedRedstonerProZ",
            role = UserRole.PROFESSOR,
            email = "maskedredstonerproz@gmail.com",
            password = "c3%>MW;P7~gqX_B}?@AwyG"
        )

        service.signUp(user)

        service.updateUser(user.email, listOf("654ac77f7f77631004df85bb"))

        assertThat(repository.users[0].createdQuizzes).isNotNull()
    }

    @Test
    fun `update user with createdQuizzes list and a blank email, not updated, UserEmailBlank exception is thrown`() = testApplication {

        assertThrows(UserBlankEmailException::class.java) {
            testApplication {
                service.updateUser("", listOf("654ac77f7f77631004df85bb"))
            }
        }

    }

    @Test
    fun `update user with empty createdQuizzes list, not updated, UserEmptyCreatedQuizzesList exception is thrown`() = testApplication {

        val user = User(
            name = "MaskedRedstonerProZ",
            role = UserRole.PROFESSOR,
            email = "maskedredstonerproz@gmail.com",
            password = "c3%>MW;P7~gqX_B}?@AwyG"
        )

        service.signUp(user)

        assertThrows(UserEmptyCreatedQuizzesListException::class.java) {
            testApplication {
                service.updateUser(user.email, emptyList())
            }
        }

    }

    @Test
    fun `update user with createdQuizzes list that has an empty string, not updated, UserCreatedQuizzesListHasEmptyString exception is thrown`() = testApplication {

        val user = User(
            name = "MaskedRedstonerProZ",
            role = UserRole.PROFESSOR,
            email = "maskedredstonerproz@gmail.com",
            password = "c3%>MW;P7~gqX_B}?@AwyG"
        )

        service.signUp(user)

        assertThrows(UserCreatedQuizzesListHasEmptyStringException::class.java) {
            testApplication {
                service.updateUser(user.email, listOf(""))
            }
        }

    }

    @Test
    fun `update user with assignedQuizzes list, success`() = testApplication {

        val user = User(
            name = "MaskedRedstonerProZ",
            role = UserRole.STUDENT,
            email = "maskedredstonerproz@gmail.com",
            password = "c3%>MW;P7~gqX_B}?@AwyG"
        )

        service.signUp(user)

        service.updateUser(user.email, assignedQuizzes = listOf("654ac77f7f77631004df85bb"))

        assertThat(repository.users[0].assignedQuizzes).isNotNull()
    }

    @Test
    fun `update user with assignedQuizzes list and a blank email, not updated, UserEmailBlank exception is thrown`() = testApplication {

        assertThrows(UserBlankEmailException::class.java) {
            testApplication {
                service.updateUser("", assignedQuizzes = listOf("654ac77f7f77631004df85bb"))
            }
        }

    }

    @Test
    fun `update user with empty assignedQuizzes list, not updated, UserEmptyAssignedQuizzesList exception is thrown`() = testApplication {

        val user = User(
            name = "MaskedRedstonerProZ",
            role = UserRole.STUDENT,
            email = "maskedredstonerproz@gmail.com",
            password = "c3%>MW;P7~gqX_B}?@AwyG"
        )

        service.signUp(user)

        assertThrows(UserEmptyAssignedQuizzesListException::class.java) {
            testApplication {
                service.updateUser(user.email, assignedQuizzes = emptyList())
            }
        }

    }

    @Test
    fun `update user with assignedQuizzes list that has an empty string, not updated, UserAssignedQuizzesListHasEmptyString exception is thrown`() = testApplication {

        val user = User(
            name = "MaskedRedstonerProZ",
            role = UserRole.STUDENT,
            email = "maskedredstonerproz@gmail.com",
            password = "c3%>MW;P7~gqX_B}?@AwyG"
        )

        service.signUp(user)

        assertThrows(UserAssignedQuizzesListHasEmptyStringException::class.java) {
            testApplication {
                service.updateUser(user.email, assignedQuizzes = listOf(""))
            }
        }

    }
}