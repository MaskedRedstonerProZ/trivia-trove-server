package com.onrender.maskedredstonerproz.services

import com.google.common.truth.Truth.assertThat
import com.onrender.maskedredstonerproz.data.models.Question
import com.onrender.maskedredstonerproz.data.models.exceptions.question.QuestionAnswerIdEmptyStringException
import com.onrender.maskedredstonerproz.data.models.exceptions.question.QuestionTextEmptyStringException
import com.onrender.maskedredstonerproz.data.repos.FakeQuestionRepository
import io.ktor.server.testing.*
import org.junit.Assert.assertThrows
import org.junit.Before
import org.junit.Test

class QuestionServiceTest {

    private lateinit var repository: FakeQuestionRepository
    private lateinit var service: QuestionService

    @Before
    fun setUp() {
        repository = FakeQuestionRepository()
        service = QuestionService(repository)
    }

    @Test
    fun `create question with valid data, success`() = testApplication {

        val question = Question(
            text = "Prvi programer bila je Ada Lovelace",
            answerId = "654a7c6cc1d7d17221bdb475"
        )

        service.createQuestion(question)

        assertThat(repository.questions).contains(question)

    }

    @Test
    fun `create question with text that is an empty string, not created, QuestionTextEmptyString exception is thrown`() = testApplication {

        val question = Question(
            text = "",
            answerId = "654a7c6cc1d7d17221bdb475"
        )

        assertThrows(QuestionTextEmptyStringException::class.java) {
            testApplication {
                service.createQuestion(question)
            }
        }

    }

    @Test
    fun `create question with answerId that is an empty string, not created, QuestionAnswerIdEmptyString exception is thrown`() = testApplication {

        val question = Question(
            text = "Prvi programer bila je Ada Lovelace",
            answerId = ""
        )

        assertThrows(QuestionAnswerIdEmptyStringException::class.java) {
            testApplication {
                service.createQuestion(question)
            }
        }

    }

}