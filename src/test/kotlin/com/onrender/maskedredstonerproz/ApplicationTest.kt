package com.onrender.maskedredstonerproz

//import com.google.common.truth.Truth.assertThat
//import com.onrender.maskedredstonerproz.data.repos.FakeUserRepository
//import com.onrender.maskedredstonerproz.data.requests.UserSignUpRequest
//import com.onrender.maskedredstonerproz.data.responses.BasicApiResponse
//import com.onrender.maskedredstonerproz.routes.signUp
//import com.onrender.maskedredstonerproz.services.UserService
//import com.onrender.maskedredstonerproz.util.UserRole
//import io.ktor.client.call.*
//import io.ktor.client.request.*
//import io.ktor.http.*
//import io.ktor.serialization.kotlinx.json.*
//import io.ktor.server.plugins.contentnegotiation.*
//import io.ktor.server.testing.*
//import org.junit.Before
//import kotlin.test.Test
//
//class ApplicationTest {
//
//    private lateinit var repository: FakeUserRepository
//    private lateinit var service: UserService
//
//    @Before
//    fun setUp() {
//        repository = FakeUserRepository()
//        service = UserService(repository)
//    }
//
//    @Test
//    fun `calls the user signup route with valid data, user signed up`() = testApplication {
//        routing {
//            signUp(service)
//        }
//
//        val client = createClient {
//            this@testApplication.install(ContentNegotiation) {
//                json()
//            }
//        }
//
//        client.post("/api/user/signup") {
//            contentType(ContentType.Application.Json)
//            setBody(
//                UserSignUpRequest(
//                    name = "MaskedRedstonerProZ",
//                    role = UserRole.PROFESSOR,
//                    email = "maskedredstonerproz@gail.com",
//                    password = "c3%>MW;P7~gqX_B}?@AwyG"
//                )
//            )
//        }.apply {
//            assertThat(status).isEqualTo(HttpStatusCode.OK)
//            assertThat(body<BasicApiResponse<Unit>>().successful).isTrue()
//        }
//    }
//}