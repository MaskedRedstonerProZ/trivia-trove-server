package com.onrender.maskedredstonerproz.util

import com.google.common.truth.Truth.assertThat
import com.onrender.maskedredstonerproz.data.models.BlankPosition
import org.junit.Test

class ListExtTest {

    @Test
    fun `calls the makePositionsOfBlanks function, positions correctly returned`() {

        val sentences = listOf(
            "Hello world"
        )

        val positions = sentences.makePositionsOfBlanks("world")

        assertThat(positions[0]).isEqualTo(BlankPosition(sentences[0].indexOf('w'), sentences[0].indexOf('d')))
    }

}