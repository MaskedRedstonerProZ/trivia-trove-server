package com.onrender.maskedredstonerproz.data.repos

import com.onrender.maskedredstonerproz.data.models.Answer

class FakeAnswerRepository: AnswerRepository {

    val answers: ArrayList<Answer> = arrayListOf()

    override suspend fun createAnswer(answer: Answer) {
        answers.add(answer)
    }

    override suspend fun getAnswersByIds(answerIds: List<String>): List<Answer?> = answers.filter { answerIds.any { id -> id == it.id } }

    override suspend fun deleteAnswer(answerId: String) {
        answers.removeIf { it.id == answerId }
    }


}