package com.onrender.maskedredstonerproz.data.repos

import com.onrender.maskedredstonerproz.data.models.Question

class FakeQuestionRepository: QuestionRepository {

    val questions: ArrayList<Question> = arrayListOf()

    override suspend fun createQuestion(question: Question) {
        questions.add(question)
    }

    override suspend fun getQuestionsByIds(questionIds: List<String>): List<Question?> = questions.filter { questionIds.any { id -> id == it.id } }

    override suspend fun deleteQuestion(questionId: String) {
        questions.removeIf { it.id == questionId }
    }


}