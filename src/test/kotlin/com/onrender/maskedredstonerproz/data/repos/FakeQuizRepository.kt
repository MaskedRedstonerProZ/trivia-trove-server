package com.onrender.maskedredstonerproz.data.repos

import com.onrender.maskedredstonerproz.data.models.Quiz

class FakeQuizRepository: QuizRepository {

    val quizzes: ArrayList<Quiz> = arrayListOf()

    override suspend fun createQuiz(quiz: Quiz) {
        quizzes.add(quiz)
    }

    override suspend fun getQuizzesByIds(quizIds: List<String>): List<Quiz?> = quizzes.filter { quizIds.any { id -> id == it.id } }

    override suspend fun deleteQuiz(quizId: String) {
        quizzes.removeIf { it.id == quizId }
    }


}