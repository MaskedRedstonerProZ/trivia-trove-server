package com.onrender.maskedredstonerproz.data.repos

import com.onrender.maskedredstonerproz.data.models.User
import org.mindrot.jbcrypt.BCrypt

class FakeUserRepository: UserRepository {

    val users: ArrayList<User> = arrayListOf()

    override suspend fun createUser(user: User) {
        users.add(user.copy(
            password = BCrypt.hashpw(user.password, BCrypt.gensalt(5))
        ))
    }

    override suspend fun getUserById(id: String): User? = users.find { it.id == id }

    override suspend fun getUserByEmail(email: String): User? = users.find { it.email == email }

    override suspend fun updateUser(email: String, createdQuizzes: List<String>?, assignedQuizzes: List<String>?) {

        val user = getUserByEmail(email) ?: return

        users[users.indexOf(user)] = user.copy(
            createdQuizzes = createdQuizzes,
            assignedQuizzes = assignedQuizzes
        )

    }

    override suspend fun doesEmailBelongToUserId(email: String, id: String): Boolean = getUserById(id)?.email == email

    override fun isPasswordValid(enteredPassword: String, actualPassword: String): Boolean {
        return BCrypt.checkpw(enteredPassword, actualPassword)
    }
}