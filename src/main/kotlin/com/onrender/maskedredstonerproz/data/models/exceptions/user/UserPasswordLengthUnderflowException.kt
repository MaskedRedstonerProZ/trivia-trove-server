package com.onrender.maskedredstonerproz.data.models.exceptions.user

import com.onrender.maskedredstonerproz.util.Constants

class UserPasswordLengthUnderflowException: Exception("The password of the user must be ${Constants.MINIMUM_PASSWORD_LENGTH} characters long!")