package com.onrender.maskedredstonerproz.data.models

import kotlinx.serialization.Serializable
import org.bson.codecs.pojo.annotations.BsonId
import org.bson.types.ObjectId

@Serializable
data class Quiz(
    val title: String,
    val description: String,
    val questionIds: List<String>,
    @BsonId
    val id: String = ObjectId().toString(),
)
