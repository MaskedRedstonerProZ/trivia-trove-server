package com.onrender.maskedredstonerproz.data.models.exceptions.answer.single_choice

class AnswerCorrectAnswerChoiceEmptyStringException: Exception("The Answer's correct choice must not be an empty string!")
