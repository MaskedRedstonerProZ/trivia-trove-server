package com.onrender.maskedredstonerproz.data.models.exceptions.answer.match_descriptions

class AnswerEmptyDescriptionsListException: Exception("The Answer's descriptions list must not be empty!")