package com.onrender.maskedredstonerproz.data.models.exceptions.answer.single_choice

class AnswerChoiceListHasEmptyStringException: Exception("The Answer's choices list must not contain an empty string!")
