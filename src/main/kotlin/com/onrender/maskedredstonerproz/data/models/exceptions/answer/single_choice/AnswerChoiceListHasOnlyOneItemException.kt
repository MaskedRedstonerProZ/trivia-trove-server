package com.onrender.maskedredstonerproz.data.models.exceptions.answer.single_choice

class AnswerChoiceListHasOnlyOneItemException: Exception("The Answer's choice list must contain at least 2 choices")