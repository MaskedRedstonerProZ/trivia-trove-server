package com.onrender.maskedredstonerproz.data.models.exceptions.question

class QuestionTextEmptyStringException: Exception("The Question's text must not be an empty string!")