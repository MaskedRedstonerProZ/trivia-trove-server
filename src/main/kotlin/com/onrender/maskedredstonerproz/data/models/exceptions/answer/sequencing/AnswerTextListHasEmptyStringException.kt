package com.onrender.maskedredstonerproz.data.models.exceptions.answer.sequencing

class AnswerTextListHasEmptyStringException: Exception("The Answer's text list must not contain an empty string!")