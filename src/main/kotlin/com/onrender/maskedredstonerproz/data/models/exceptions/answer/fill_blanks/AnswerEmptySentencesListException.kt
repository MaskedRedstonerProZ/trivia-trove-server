package com.onrender.maskedredstonerproz.data.models.exceptions.answer.fill_blanks

class AnswerEmptySentencesListException: Exception("The Answer's sentences list must not be empty!")