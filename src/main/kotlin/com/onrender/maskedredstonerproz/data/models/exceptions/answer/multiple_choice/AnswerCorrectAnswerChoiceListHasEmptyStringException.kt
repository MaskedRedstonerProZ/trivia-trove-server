package com.onrender.maskedredstonerproz.data.models.exceptions.answer.multiple_choice

class AnswerCorrectAnswerChoiceListHasEmptyStringException: Exception("The Answer's choices list must not contain an empty string!")
