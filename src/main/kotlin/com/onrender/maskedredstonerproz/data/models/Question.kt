package com.onrender.maskedredstonerproz.data.models

import kotlinx.serialization.Serializable
import org.bson.codecs.pojo.annotations.BsonId
import org.bson.types.ObjectId

@Serializable
data class Question(
    val text: String,
    val answerId: String,
    @BsonId
    val id: String = ObjectId().toString()
)
