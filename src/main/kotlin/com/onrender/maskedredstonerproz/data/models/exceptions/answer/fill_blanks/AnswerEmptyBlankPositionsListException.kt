package com.onrender.maskedredstonerproz.data.models.exceptions.answer.fill_blanks

class AnswerEmptyBlankPositionsListException: Exception("The Answer's list of positions of the blanks must not be empty!")