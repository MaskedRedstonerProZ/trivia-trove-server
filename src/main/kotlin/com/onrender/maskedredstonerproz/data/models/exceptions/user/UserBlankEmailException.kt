package com.onrender.maskedredstonerproz.data.models.exceptions.user

class UserBlankEmailException: Exception("The email of the user cannot be blank!")