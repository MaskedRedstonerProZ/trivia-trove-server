package com.onrender.maskedredstonerproz.data.models.exceptions.answer.multiple_choice

class AnswerCorrectAnswerChoiceListItemUnderflowException: Exception("The Answer's correct answer choice list must have at least 2 items!")