package com.onrender.maskedredstonerproz.data.models.exceptions.answer.fill_blanks

class AnswerUnspecifiedBlankPositionsException: Exception("The Answer's blank positions must be specified for each sentence!")