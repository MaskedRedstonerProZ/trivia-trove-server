package com.onrender.maskedredstonerproz.data.models.exceptions.answer.match_descriptions

class AnswerDescriptionsListHasEmptyStringException: Exception("The Answer's descriptions list must not contain an empty string!")
