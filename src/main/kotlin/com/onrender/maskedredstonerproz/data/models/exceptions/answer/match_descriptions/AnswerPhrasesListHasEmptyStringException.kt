package com.onrender.maskedredstonerproz.data.models.exceptions.answer.match_descriptions

class AnswerPhrasesListHasEmptyStringException: Exception("The Answer's phrases list must not contain an empty string!")
