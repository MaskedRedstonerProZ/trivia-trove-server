package com.onrender.maskedredstonerproz.data.models.exceptions.answer.multiple_choice

class AnswerChoiceListItemUnderflowException: Exception("The Answer's choice list must have at least 3 items!")