package com.onrender.maskedredstonerproz.data.models.exceptions.user

class UserPasswordLacksCapitalLetterException: Exception("The password of the user must have at least one capital letter!") {
}