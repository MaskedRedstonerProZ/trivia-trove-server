package com.onrender.maskedredstonerproz.data.models.exceptions.quiz

class QuizQuestionIdsListHasEmptyStringException: Exception("The Quiz's question ids list must not contain an empty string!")