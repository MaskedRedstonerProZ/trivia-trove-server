package com.onrender.maskedredstonerproz.data.models.exceptions.answer.multiple_choice

class AnswerEmptyChoiceListException: Exception("The Answer's choices list must not be empty!")
