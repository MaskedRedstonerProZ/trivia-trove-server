package com.onrender.maskedredstonerproz.data.models.exceptions.user

class UserAssignedQuizzesListHasEmptyStringException: Exception("The User's assigned quizzes list must not contain an empty string!")
