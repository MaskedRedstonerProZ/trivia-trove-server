package com.onrender.maskedredstonerproz.data.models

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import kotlinx.serialization.Serializable
import org.bson.codecs.pojo.annotations.BsonId
import org.bson.types.ObjectId

typealias BlankPosition = Pair<Int, Int>

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes(
    JsonSubTypes.Type(value = Answer.SingleChoice::class, name = "SingleChoice"),
    JsonSubTypes.Type(value = Answer.MultipleChoice::class, name = "MultipleChoice"),
    JsonSubTypes.Type(value = Answer.TrueFalse::class, name = "TrueFalse"),
    JsonSubTypes.Type(value = Answer.FillTheBlanks::class, name = "FillTheBlanks"),
    JsonSubTypes.Type(value = Answer.MatchDescriptions::class, name = "MatchDescriptions"),
    JsonSubTypes.Type(value = Answer.Sequencing::class, name = "Sequencing")
)
@Serializable
sealed class Answer(
    @BsonId
    val id: String = ObjectId().toString(),
) {

    abstract val type: String

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
    @JsonSubTypes(
        JsonSubTypes.Type(value = SingleChoice::class, name = "SingleChoice")
    )
    @Serializable
    data class SingleChoice(
        val choices: List<String>,
        val correctAnswerChoice: String,
        override val type: String = "SingleChoice" // Add the type attribute to the subclass
    ) : Answer()

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
    @JsonSubTypes(
        JsonSubTypes.Type(value = MultipleChoice::class, name = "MultipleChoice")
    )
    @Serializable
    data class MultipleChoice(
        val choices: List<String>,
        val correctAnswerChoices: List<String>,
        override val type: String = "MultipleChoice" // Add the type attribute to the subclass
    ) : Answer()

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
    @JsonSubTypes(
        JsonSubTypes.Type(value = TrueFalse::class, name = "TrueFalse")
    )
    @Serializable
    data class TrueFalse(
        val isTrue: Boolean,
        override val type: String = "TrueFalse" // Add the type attribute to the subclass
    ) : Answer()

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
    @JsonSubTypes(
        JsonSubTypes.Type(value = FillTheBlanks::class, name = "FillTheBlanks")
    )
    @Serializable
    data class FillTheBlanks(
        val sentences: List<String>,
        val positionsOfBlanks: List<BlankPosition>,
        override val type: String = "FillTheBlanks" // Add the type attribute to the subclass
    ) : Answer()

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
    @JsonSubTypes(
        JsonSubTypes.Type(value = MatchDescriptions::class, name = "MatchDescriptions")
    )
    @Serializable
    data class MatchDescriptions(
        val phrases: List<String>,
        val descriptions: List<String>,
        override val type: String = "MatchDescriptions" // Add the type attribute to the subclass
    ) : Answer()

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
    @JsonSubTypes(
        JsonSubTypes.Type(value = Sequencing::class, name = "Sequencing")
    )
    @Serializable
    data class Sequencing(
        val answerTextsInCorrectSequence: List<String>,
        override val type: String = "Sequencing" // Add the type attribute to the subclass
    ) : Answer()
}
