package com.onrender.maskedredstonerproz.data.models.exceptions.quiz

class QuizEmptyQuestionIdsListException: Exception("The Quiz's question ids list must not be empty!")