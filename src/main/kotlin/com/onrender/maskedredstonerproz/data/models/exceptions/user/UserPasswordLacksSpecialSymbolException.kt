package com.onrender.maskedredstonerproz.data.models.exceptions.user

class UserPasswordLacksSpecialSymbolException: Exception("The password of the user must have at least one of the following special symbols: !\";#\$%&'()*+,-./:;<=>?@[]^_`{|}~") {
}