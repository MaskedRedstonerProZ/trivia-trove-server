package com.onrender.maskedredstonerproz.data.models

import kotlinx.serialization.Serializable
import org.bson.codecs.pojo.annotations.BsonId
import org.bson.types.ObjectId

@Serializable
data class User(
    val name: String,
    val role: Int,
    val email: String,
    val password: String,
    val createdQuizzes: List<String>? = null,
    val assignedQuizzes: List<String>? = null,
    @BsonId
    val id: String = ObjectId().toString(),
)
