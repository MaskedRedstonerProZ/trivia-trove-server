package com.onrender.maskedredstonerproz.data.models.exceptions.user

class UserPasswordLacksNumberException: Exception("The password of the user must have at least one number!") {
}