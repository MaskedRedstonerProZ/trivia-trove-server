package com.onrender.maskedredstonerproz.data.models.exceptions.user

class UserPasswordHasSequencesException: Exception("The password of the user must not contain sequences such as 'abc', '123'...etc.") {
}