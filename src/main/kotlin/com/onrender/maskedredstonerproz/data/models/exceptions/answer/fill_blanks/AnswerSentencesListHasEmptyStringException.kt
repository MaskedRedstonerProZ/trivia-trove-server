package com.onrender.maskedredstonerproz.data.models.exceptions.answer.fill_blanks

class AnswerSentencesListHasEmptyStringException: Exception("The Answer's sentences list must not contain an empty string!")