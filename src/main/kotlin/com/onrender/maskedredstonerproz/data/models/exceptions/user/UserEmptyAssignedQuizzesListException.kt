package com.onrender.maskedredstonerproz.data.models.exceptions.user

class UserEmptyAssignedQuizzesListException: Exception("The User's assigned quizzes list must not be empty!")
