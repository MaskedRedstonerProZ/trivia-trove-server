package com.onrender.maskedredstonerproz.data.models.exceptions.user

class UserInvalidRoleException: Exception("The role of the user must be either UserRole.PROFESSOR or UserRole.STUDENT!")