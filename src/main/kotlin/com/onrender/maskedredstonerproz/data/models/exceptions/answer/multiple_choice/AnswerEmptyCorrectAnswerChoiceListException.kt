package com.onrender.maskedredstonerproz.data.models.exceptions.answer.multiple_choice

class AnswerEmptyCorrectAnswerChoiceListException: Exception("The Answer's correct choices list must not be empty!")
