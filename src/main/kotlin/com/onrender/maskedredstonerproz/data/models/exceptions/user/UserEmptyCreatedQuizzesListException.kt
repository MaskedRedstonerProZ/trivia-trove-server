package com.onrender.maskedredstonerproz.data.models.exceptions.user

class UserEmptyCreatedQuizzesListException: Exception("The User's created quizzes list must not be empty!")
