package com.onrender.maskedredstonerproz.data.models.exceptions.answer.match_descriptions

class AnswerEmptyPhrasesListException: Exception("The Answer's phrases list must not be empty!")