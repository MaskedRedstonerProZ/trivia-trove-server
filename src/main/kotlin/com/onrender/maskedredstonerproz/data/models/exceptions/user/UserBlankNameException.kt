package com.onrender.maskedredstonerproz.data.models.exceptions.user

class UserBlankNameException: Exception("The name of the user cannot be blank!")