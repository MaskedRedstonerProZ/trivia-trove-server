package com.onrender.maskedredstonerproz.data.models.exceptions.answer.single_choice

class AnswerEmptyChoiceListException: Exception("The Answer's choices list must not be empty!")
