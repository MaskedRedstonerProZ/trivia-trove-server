package com.onrender.maskedredstonerproz.data.models.exceptions.answer.sequencing

class AnswerEmptyTextListException: Exception("The Answer's text list must not be empty!")
