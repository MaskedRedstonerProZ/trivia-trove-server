package com.onrender.maskedredstonerproz.data.models.exceptions.user

class UserCreatedQuizzesListHasEmptyStringException: Exception("The User's created quizzes list must not contain an empty string!")
