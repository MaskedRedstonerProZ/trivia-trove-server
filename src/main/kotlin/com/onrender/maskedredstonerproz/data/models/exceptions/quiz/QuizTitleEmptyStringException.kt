package com.onrender.maskedredstonerproz.data.models.exceptions.quiz

class QuizTitleEmptyStringException: Exception("The Quiz's title must not be an empty string!")