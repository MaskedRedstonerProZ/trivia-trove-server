package com.onrender.maskedredstonerproz.data.models.exceptions.user

class UserInvalidEmailException: Exception("The email of the user must be valid!")