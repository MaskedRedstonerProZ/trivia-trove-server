package com.onrender.maskedredstonerproz.data.models.exceptions.user

class UserPasswordHasRepeatedCharactersException: Exception("The password of the user must not contain repeated characters!") {
}