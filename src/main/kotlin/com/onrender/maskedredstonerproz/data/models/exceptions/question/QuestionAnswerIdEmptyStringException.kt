package com.onrender.maskedredstonerproz.data.models.exceptions.question

class QuestionAnswerIdEmptyStringException: Exception("The Question's answer id must not be an empty string!")