package com.onrender.maskedredstonerproz.data.responses

import com.onrender.maskedredstonerproz.data.models.User
import kotlinx.serialization.Serializable

sealed class LoginResponse {

    @Serializable
    data class WebLoginResponse(
        val user: User
    )

    @Serializable
    data class AndroidLoginResponse(
        val id: String,
        val token: String
    )

}
