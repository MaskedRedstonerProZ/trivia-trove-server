package com.onrender.maskedredstonerproz.data.responses

import kotlinx.serialization.Serializable

@Serializable
data class QuestionResponse(
    val id: String
)
