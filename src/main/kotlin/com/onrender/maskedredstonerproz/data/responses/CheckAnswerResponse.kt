package com.onrender.maskedredstonerproz.data.responses

import kotlinx.serialization.Serializable

@Serializable
data class CheckAnswerResponse(
    val answerId: String,
    val isCorrect: Boolean
)
