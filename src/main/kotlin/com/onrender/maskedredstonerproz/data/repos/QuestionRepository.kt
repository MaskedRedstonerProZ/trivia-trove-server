package com.onrender.maskedredstonerproz.data.repos

import com.onrender.maskedredstonerproz.data.models.Question

interface QuestionRepository {

    suspend fun createQuestion(question: Question)

    suspend fun getQuestionsByIds(questionIds: List<String>): List<Question?>

    suspend fun deleteQuestion(questionId: String)

}