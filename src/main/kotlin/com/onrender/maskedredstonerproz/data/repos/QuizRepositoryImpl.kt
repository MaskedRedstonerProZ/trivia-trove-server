package com.onrender.maskedredstonerproz.data.repos

import com.onrender.maskedredstonerproz.data.models.Quiz
import org.litote.kmongo.coroutine.CoroutineDatabase

class QuizRepositoryImpl(
    db: CoroutineDatabase
): QuizRepository {

    private val quizzes = db.getCollection<Quiz>()

    override suspend fun createQuiz(quiz: Quiz) {
        quizzes.insertOne(quiz)
    }

    override suspend fun getQuizzesByIds(quizIds: List<String>): List<Quiz?> {
        val listToReturn: ArrayList<Quiz?> = arrayListOf()

        quizIds.forEach {
            listToReturn.add(quizzes.findOneById(it))
        }

        return listToReturn
    }

    override suspend fun deleteQuiz(quizId: String) {
        quizzes.deleteOneById(quizId)
    }
}