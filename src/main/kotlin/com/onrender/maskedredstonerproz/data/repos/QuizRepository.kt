package com.onrender.maskedredstonerproz.data.repos

import com.onrender.maskedredstonerproz.data.models.Quiz

interface QuizRepository {

    suspend fun createQuiz(quiz: Quiz)

    suspend fun getQuizzesByIds(quizIds: List<String>): List<Quiz?>

    suspend fun deleteQuiz(quizId: String)

}