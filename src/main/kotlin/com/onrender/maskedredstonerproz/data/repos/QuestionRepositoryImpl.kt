package com.onrender.maskedredstonerproz.data.repos

import com.onrender.maskedredstonerproz.data.models.Question
import org.litote.kmongo.coroutine.CoroutineDatabase

class QuestionRepositoryImpl(
    db: CoroutineDatabase
): QuestionRepository {

    private val questions = db.getCollection<Question>()

    override suspend fun createQuestion(question: Question) {
        questions.insertOne(question)
    }

    override suspend fun getQuestionsByIds(questionIds: List<String>): List<Question?> {
        val listToReturn: ArrayList<Question?> = arrayListOf()

        questionIds.forEach {
            listToReturn.add(questions.findOneById(it))
        }

        return listToReturn
    }

    override suspend fun deleteQuestion(questionId: String) {
        questions.deleteOneById(questionId)
    }
}