package com.onrender.maskedredstonerproz.data.repos

import com.onrender.maskedredstonerproz.data.models.Answer

interface AnswerRepository {

    suspend fun createAnswer(answer: Answer)

    suspend fun getAnswersByIds(answerIds: List<String>): List<Answer?>

    suspend fun deleteAnswer(answerId: String)

}