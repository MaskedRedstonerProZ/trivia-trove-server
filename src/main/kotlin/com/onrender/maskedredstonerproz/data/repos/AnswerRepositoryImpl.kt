package com.onrender.maskedredstonerproz.data.repos

import com.onrender.maskedredstonerproz.data.models.Answer
import org.litote.kmongo.coroutine.CoroutineDatabase

class AnswerRepositoryImpl(db: CoroutineDatabase) : AnswerRepository {

    private val answers = db.getCollection<Answer>()

    override suspend fun createAnswer(answer: Answer) {
        answers.insertOne(answer)
    }

    override suspend fun getAnswersByIds(answerIds: List<String>): List<Answer?> {
        val listToReturn: ArrayList<Answer?> = arrayListOf()

        answerIds.forEach {
            listToReturn.add(answers.findOneById(it))
        }

        return listToReturn
    }

    override suspend fun deleteAnswer(answerId: String) {
        answers.deleteOneById(answerId)
    }
}