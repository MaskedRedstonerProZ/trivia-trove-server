package com.onrender.maskedredstonerproz.data.repos

import com.onrender.maskedredstonerproz.data.models.User

interface UserRepository {

    suspend fun createUser(user: User)

    suspend fun getUserById(id: String): User?

    suspend fun getUserByEmail(email: String): User?

    suspend fun updateUser(email: String, createdQuizzes: List<String>? = null, assignedQuizzes: List<String>? = null)

    suspend fun doesEmailBelongToUserId(email: String, id: String): Boolean

    fun isPasswordValid(enteredPassword: String, actualPassword: String): Boolean
}