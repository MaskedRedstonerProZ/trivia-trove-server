package com.onrender.maskedredstonerproz.data.repos

import com.onrender.maskedredstonerproz.data.models.User
import org.litote.kmongo.coroutine.CoroutineDatabase
import org.litote.kmongo.eq
import org.mindrot.jbcrypt.BCrypt

class UserRepositoryImpl(db: CoroutineDatabase): UserRepository {

    private val users = db.getCollection<User>()

    override suspend fun createUser(user: User) {
        users.insertOne(user.copy(
            password = BCrypt.hashpw(user.password, BCrypt.gensalt(5))
        ))
    }

    override suspend fun getUserById(id: String): User? = users.findOneById(id)

    override suspend fun getUserByEmail(email: String): User? = users.findOne(User::email eq email)

    override suspend fun updateUser(email: String, createdQuizzes: List<String>?, assignedQuizzes: List<String>?) {

        val user = getUserByEmail(email) ?: return

        users.updateOneById(
            user.id,
            user.copy(
                createdQuizzes = createdQuizzes,
                assignedQuizzes = assignedQuizzes
            )
        )

    }

    override suspend fun doesEmailBelongToUserId(email: String, id: String): Boolean = getUserById(id)?.email == email

    override fun isPasswordValid(enteredPassword: String, actualPassword: String): Boolean {
        return BCrypt.checkpw(enteredPassword, actualPassword)
    }
}