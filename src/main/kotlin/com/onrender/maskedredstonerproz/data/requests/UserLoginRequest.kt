package com.onrender.maskedredstonerproz.data.requests

import kotlinx.serialization.Serializable

@Serializable
data class UserLoginRequest(
    val platformOfOrigin: String,
    val email: String,
    val password: String
)
