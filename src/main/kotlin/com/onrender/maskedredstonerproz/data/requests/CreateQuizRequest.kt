package com.onrender.maskedredstonerproz.data.requests

import kotlinx.serialization.Serializable

@Serializable
data class CreateQuizRequest(
    val title: String,
    val description: String,
    val questionIds: List<String>
)
