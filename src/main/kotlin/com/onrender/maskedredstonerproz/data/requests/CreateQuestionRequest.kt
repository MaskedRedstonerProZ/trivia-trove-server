package com.onrender.maskedredstonerproz.data.requests

import kotlinx.serialization.Serializable

@Serializable
data class CreateQuestionRequest(
    val text: String,
    val answerId: String
)