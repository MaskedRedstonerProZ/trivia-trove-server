package com.onrender.maskedredstonerproz.data.requests

import com.onrender.maskedredstonerproz.data.models.BlankPosition
import com.onrender.maskedredstonerproz.util.serializer.CreateAnswerRequestSerializer
import kotlinx.serialization.Serializable

@Serializable(with = CreateAnswerRequestSerializer::class)
sealed interface CreateAnswerRequest {

    val type: String

    @Serializable
    data class SingleChoice(
        val choices: List<String>,
        val correctAnswerChoice: String,
        override val type: String = "sc"
    ): CreateAnswerRequest

    @Serializable
    data class MultipleChoice(
        val choices: List<String>,
        val correctAnswerChoices: List<String>,
        override val type: String = "mc"
    ): CreateAnswerRequest

    @Serializable
    data class TrueFalse(
        val isTrue: Boolean,
        override val type: String = "tf"
    ): CreateAnswerRequest

    @Serializable
    data class FillTheBlanks(
        val sentences: List<String>,
        val positionsOfBlanks: List<BlankPosition>,
        override val type: String = "fb"
    ): CreateAnswerRequest

    @Serializable
    data class MatchDescriptions(
        val phrases: List<String>,
        val descriptions: List<String>,
        override val type: String = "md"
    ): CreateAnswerRequest

    @Serializable
    data class Sequencing(
        val answerTextsInCorrectSequence: List<String>,
        override val type: String = "sqc"
    ): CreateAnswerRequest
}