package com.onrender.maskedredstonerproz.data.requests

import kotlinx.serialization.Serializable

@Serializable
data class UserAddCreatedQuizRequest(
    val email: String,
    val quizId: String
)
