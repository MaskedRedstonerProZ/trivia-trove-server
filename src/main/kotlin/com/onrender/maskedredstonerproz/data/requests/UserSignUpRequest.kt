package com.onrender.maskedredstonerproz.data.requests

import kotlinx.serialization.Serializable

@Serializable
data class UserSignUpRequest(
    val name: String,
    val role: Int,
    val email: String,
    val password: String
)
