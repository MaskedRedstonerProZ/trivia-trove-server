package com.onrender.maskedredstonerproz.data.requests

import com.onrender.maskedredstonerproz.data.models.BlankPosition
import com.onrender.maskedredstonerproz.util.serializer.CheckAnswerRequestSerializer
import kotlinx.serialization.Serializable

@Serializable(with = CheckAnswerRequestSerializer::class)
sealed interface CheckAnswerRequest {

    val answerId: String
    val type: String

    @Serializable
    data class SingleChoice(
        val choices: List<String>,
        val correctAnswerChoice: String,
        override val answerId: String,
        override val type: String = "sc"
    ): CheckAnswerRequest

    @Serializable
    data class MultipleChoice(
        val choices: List<String>,
        val correctAnswerChoices: List<String>,
        override val answerId: String,
        override val type: String = "mc"
    ): CheckAnswerRequest

    @Serializable
    data class TrueFalse(
        val isTrue: Boolean,
        override val answerId: String,
        override val type: String = "tf"
    ): CheckAnswerRequest

    @Serializable
    data class FillTheBlanks(
        val sentences: List<String>,
        val positionsOfBlanks: List<BlankPosition>,
        override val answerId: String,
        override val type: String = "fb"
    ): CheckAnswerRequest

    @Serializable
    data class MatchDescriptions(
        val phrases: List<String>,
        val descriptions: List<String>,
        override val answerId: String,
        override val type: String = "md"
    ): CheckAnswerRequest

    @Serializable
    data class Sequencing(
        val answerTextsInCorrectSequence: List<String>,
        override val answerId: String,
        override val type: String = "sqc"
    ): CheckAnswerRequest
}