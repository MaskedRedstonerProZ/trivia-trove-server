package com.onrender.maskedredstonerproz

import com.onrender.maskedredstonerproz.di.mainModule
import com.onrender.maskedredstonerproz.plugins.*
import io.ktor.server.application.*

fun main(args: Array<String>) {
    io.ktor.server.netty.EngineMain.main(args)
}

@Suppress("unused")
fun Application.module() {
    configureDI(mainModule)
    configureHTTP()
    configureMonitoring()
    configureSerialization()
    configureSecurity()
    configureRouting()
}
