package com.onrender.maskedredstonerproz.services

import com.onrender.maskedredstonerproz.data.models.User
import com.onrender.maskedredstonerproz.data.models.exceptions.user.*
import com.onrender.maskedredstonerproz.data.repos.UserRepository
import com.onrender.maskedredstonerproz.util.Constants
import com.onrender.maskedredstonerproz.util.UserRole

class UserService(
    private val repository: UserRepository
) {

    suspend fun doesUserWithEmailExist(email: String): Boolean = repository.getUserByEmail(email) != null

    @Throws(
        UserBlankNameException::class,
        UserInvalidRoleException::class,
        UserBlankEmailException::class,
        UserInvalidEmailException::class,
        UserPasswordLengthUnderflowException::class,
        UserPasswordHasSequencesException::class,
        UserPasswordHasRepeatedCharactersException::class,
        UserPasswordLacksCapitalLetterException::class,
        UserPasswordLacksNumberException::class,
        UserPasswordLacksSpecialSymbolException::class
    )
    suspend fun signUp(user: User) {

        val specialSymbols = "!\";#\$%&'()*+,-./:;<=>?@[]^_`{|}~"

        val digitList = (0..9).toList().joinToString().filter { it != ' ' && it != ',' }

        val letterList = ('a'..'z').toList().joinToString().filter { it != ' ' && it != ',' }

        val capitalLetterList = ('A'..'Z').toList().joinToString().filter { it != ' ' && it != ',' }

        val emailRegex = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}\$"

        if (user.name.isBlank()) throw UserBlankNameException()

        if (user.role !in arrayOf(UserRole.PROFESSOR, UserRole.STUDENT)) throw UserInvalidRoleException()

        if (user.email.isBlank()) throw UserBlankEmailException()

        if (!emailRegex.toRegex().matches(user.email)) throw UserInvalidEmailException()

        if (user.password.length < Constants.MINIMUM_PASSWORD_LENGTH) throw UserPasswordLengthUnderflowException()

        var i = 0
        var j = 1
        while(i <= user.password.lastIndex - 1 && j <= user.password.lastIndex) {
            val c = user.password[i]
            val c1 = user.password[j]

            if (c.isDigit()) {
                if (digitList.contains("$c$c1")) throw UserPasswordHasSequencesException()
            }

            if (c.isLowerCase()) {
                if (letterList.contains("$c$c1")) throw UserPasswordHasSequencesException()
            }

            if (c.isUpperCase()) {
                if (capitalLetterList.contains("$c$c1")) throw UserPasswordHasSequencesException()
            }

            i++
            j++
        }

        user.password.substring(0..<user.password.lastIndex).forEachIndexed { index, c ->
            user.password.substring((index + 1)..user.password.lastIndex).forEach {
                if (it == c) throw UserPasswordHasRepeatedCharactersException()
            }
        }

        if (!user.password.any { it.isUpperCase() }) throw UserPasswordLacksCapitalLetterException()

        if (!user.password.any { it.isDigit() }) throw UserPasswordLacksNumberException()

        if (!user.password.any { c -> specialSymbols.any { c == it } }) throw UserPasswordLacksSpecialSymbolException()


        repository.createUser(user)
    }

    suspend fun getUserByEmail(email: String): User? = repository.getUserByEmail(email)

    suspend fun getUserById(id: String): User? = repository.getUserById(id)

    suspend fun updateUser(email: String, createdQuizzes: List<String>? = null, assignedQuizzes: List<String>? = null) {

        val user = getUserByEmail(email) ?: if(email.isEmpty()) throw UserBlankEmailException() else return

        if(createdQuizzes != null) {

            if(createdQuizzes.isEmpty()) throw UserEmptyCreatedQuizzesListException()

            createdQuizzes.forEach {
                if(it.isEmpty()) throw UserCreatedQuizzesListHasEmptyStringException()
            }

            repository.updateUser(email, if(user.createdQuizzes == null) createdQuizzes else user.createdQuizzes.plus(createdQuizzes), null)
            return
        }

        if(assignedQuizzes != null) {

            if(assignedQuizzes.isEmpty()) throw UserEmptyAssignedQuizzesListException()

            assignedQuizzes.forEach {
                if(it.isEmpty()) throw UserAssignedQuizzesListHasEmptyStringException()
            }

            repository.updateUser(email, null, if(user.assignedQuizzes == null) assignedQuizzes else user.assignedQuizzes.plus(assignedQuizzes))
            return
        }

    }

    fun isPasswordValid(enteredPassword: String, actualPassword: String): Boolean {
        return repository.isPasswordValid(enteredPassword, actualPassword)
    }
}