package com.onrender.maskedredstonerproz.services

import com.onrender.maskedredstonerproz.data.models.Answer
import com.onrender.maskedredstonerproz.data.models.exceptions.answer.fill_blanks.AnswerEmptyBlankPositionsListException
import com.onrender.maskedredstonerproz.data.models.exceptions.answer.fill_blanks.AnswerEmptySentencesListException
import com.onrender.maskedredstonerproz.data.models.exceptions.answer.fill_blanks.AnswerSentencesListHasEmptyStringException
import com.onrender.maskedredstonerproz.data.models.exceptions.answer.fill_blanks.AnswerUnspecifiedBlankPositionsException
import com.onrender.maskedredstonerproz.data.models.exceptions.answer.match_descriptions.AnswerDescriptionsListHasEmptyStringException
import com.onrender.maskedredstonerproz.data.models.exceptions.answer.match_descriptions.AnswerEmptyDescriptionsListException
import com.onrender.maskedredstonerproz.data.models.exceptions.answer.match_descriptions.AnswerEmptyPhrasesListException
import com.onrender.maskedredstonerproz.data.models.exceptions.answer.match_descriptions.AnswerPhrasesListHasEmptyStringException
import com.onrender.maskedredstonerproz.data.models.exceptions.answer.multiple_choice.AnswerChoiceListItemUnderflowException
import com.onrender.maskedredstonerproz.data.models.exceptions.answer.multiple_choice.AnswerCorrectAnswerChoiceListHasEmptyStringException
import com.onrender.maskedredstonerproz.data.models.exceptions.answer.multiple_choice.AnswerCorrectAnswerChoiceListItemUnderflowException
import com.onrender.maskedredstonerproz.data.models.exceptions.answer.multiple_choice.AnswerEmptyCorrectAnswerChoiceListException
import com.onrender.maskedredstonerproz.data.models.exceptions.answer.sequencing.AnswerEmptyTextListException
import com.onrender.maskedredstonerproz.data.models.exceptions.answer.sequencing.AnswerTextListHasEmptyStringException
import com.onrender.maskedredstonerproz.data.models.exceptions.answer.single_choice.AnswerChoiceListHasEmptyStringException
import com.onrender.maskedredstonerproz.data.models.exceptions.answer.single_choice.AnswerChoiceListHasOnlyOneItemException
import com.onrender.maskedredstonerproz.data.models.exceptions.answer.single_choice.AnswerCorrectAnswerChoiceEmptyStringException
import com.onrender.maskedredstonerproz.data.models.exceptions.answer.single_choice.AnswerEmptyChoiceListException
import com.onrender.maskedredstonerproz.data.repos.AnswerRepository
import com.onrender.maskedredstonerproz.data.models.exceptions.answer.multiple_choice.AnswerChoiceListHasEmptyStringException as AnswerMultipleChoiceChoiceListHasEmptyStringException
import com.onrender.maskedredstonerproz.data.models.exceptions.answer.multiple_choice.AnswerEmptyChoiceListException as AnswerEmptyMultipleChoiceChoiceListException

class AnswerService(
    private val repository: AnswerRepository
) {

    @Throws(
        AnswerEmptyChoiceListException::class,
        AnswerChoiceListHasEmptyStringException::class,
        AnswerCorrectAnswerChoiceEmptyStringException::class,
        AnswerChoiceListHasOnlyOneItemException::class,
        AnswerEmptyTextListException::class,
        AnswerTextListHasEmptyStringException::class
    )
    suspend fun createAnswer(answer: Answer) {

        when(answer) {

            is Answer.SingleChoice -> {
                if(answer.choices.isEmpty()) throw AnswerEmptyChoiceListException()

                answer.choices.forEach {
                    if(it.isEmpty()) throw AnswerChoiceListHasEmptyStringException()
                }

                if(answer.correctAnswerChoice.isEmpty()) throw AnswerCorrectAnswerChoiceEmptyStringException()

                if(answer.choices.count() < 2) throw AnswerChoiceListHasOnlyOneItemException()
            }

            is Answer.MultipleChoice -> {
                if(answer.choices.isEmpty()) throw AnswerEmptyMultipleChoiceChoiceListException()

                answer.choices.forEach {
                    if(it.isEmpty()) throw AnswerMultipleChoiceChoiceListHasEmptyStringException()
                }

                if(answer.choices.count() < 3) throw AnswerChoiceListItemUnderflowException()

                if(answer.correctAnswerChoices.isEmpty()) throw AnswerEmptyCorrectAnswerChoiceListException()

                answer.correctAnswerChoices.forEach {
                    if(it.isEmpty()) throw AnswerCorrectAnswerChoiceListHasEmptyStringException()
                }

                if(answer.correctAnswerChoices.count() < 2) throw AnswerCorrectAnswerChoiceListItemUnderflowException()
            }

            is Answer.TrueFalse -> Unit

            is Answer.FillTheBlanks -> {
                if(answer.sentences.isEmpty()) throw AnswerEmptySentencesListException()

                answer.sentences.forEach {
                    if(it.isEmpty()) throw AnswerSentencesListHasEmptyStringException()
                }

                if(answer.positionsOfBlanks.isEmpty()) throw AnswerEmptyBlankPositionsListException()

                if(answer.sentences.size > answer.positionsOfBlanks.size) throw AnswerUnspecifiedBlankPositionsException()
            }

            is Answer.MatchDescriptions -> {
                if(answer.phrases.isEmpty()) throw AnswerEmptyPhrasesListException()

                answer.phrases.forEach {
                    if(it.isEmpty()) throw AnswerPhrasesListHasEmptyStringException()
                }

                if(answer.descriptions.isEmpty()) throw AnswerEmptyDescriptionsListException()

                answer.descriptions.forEach {
                    if(it.isEmpty()) throw AnswerDescriptionsListHasEmptyStringException()
                }
            }

            is Answer.Sequencing -> {
                if(answer.answerTextsInCorrectSequence.isEmpty()) throw AnswerEmptyTextListException()

                answer.answerTextsInCorrectSequence.forEach {
                    if(it.isEmpty()) throw AnswerTextListHasEmptyStringException()
                }
            }
        }

        repository.createAnswer(answer)
    }

    suspend fun getAnswersByIds(vararg answerIds: String): List<Answer?> {
        return repository.getAnswersByIds(answerIds.toList())
    }

}