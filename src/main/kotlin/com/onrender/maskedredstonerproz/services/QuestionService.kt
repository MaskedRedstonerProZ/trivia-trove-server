package com.onrender.maskedredstonerproz.services

import com.onrender.maskedredstonerproz.data.models.Question
import com.onrender.maskedredstonerproz.data.models.exceptions.question.QuestionAnswerIdEmptyStringException
import com.onrender.maskedredstonerproz.data.models.exceptions.question.QuestionTextEmptyStringException
import com.onrender.maskedredstonerproz.data.repos.QuestionRepository

class QuestionService(
    private val repository: QuestionRepository
) {

    suspend fun createQuestion(question: Question) {

        if(question.text.isEmpty()) throw QuestionTextEmptyStringException()

        if(question.answerId.isEmpty()) throw QuestionAnswerIdEmptyStringException()

        repository.createQuestion(question)
    }

    suspend fun getQuestionsByIds(vararg questionIds: String): List<Question?> {
        return repository.getQuestionsByIds(questionIds.toList())
    }

}