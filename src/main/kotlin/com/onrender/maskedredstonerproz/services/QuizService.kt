package com.onrender.maskedredstonerproz.services

import com.onrender.maskedredstonerproz.data.models.Quiz
import com.onrender.maskedredstonerproz.data.models.exceptions.quiz.QuizEmptyQuestionIdsListException
import com.onrender.maskedredstonerproz.data.models.exceptions.quiz.QuizQuestionIdsListHasEmptyStringException
import com.onrender.maskedredstonerproz.data.models.exceptions.quiz.QuizTitleEmptyStringException
import com.onrender.maskedredstonerproz.data.repos.QuizRepository

class QuizService(
    private val repository: QuizRepository
) {

    suspend fun createQuiz(quiz: Quiz) {

        if(quiz.title.isEmpty()) throw QuizTitleEmptyStringException()

        if(quiz.questionIds.isEmpty()) throw QuizEmptyQuestionIdsListException()

        quiz.questionIds.forEach {
            if(it.isEmpty()) throw QuizQuestionIdsListHasEmptyStringException()
        }

        repository.createQuiz(quiz)
    }

    suspend fun getQuizzesByIds(vararg quizIds: String): List<Quiz?> {
        return repository.getQuizzesByIds(quizIds.toList())
    }

}