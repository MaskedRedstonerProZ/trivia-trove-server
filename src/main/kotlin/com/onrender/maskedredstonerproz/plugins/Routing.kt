package com.onrender.maskedredstonerproz.plugins

import com.onrender.maskedredstonerproz.data.models.Answer
import com.onrender.maskedredstonerproz.data.responses.BasicApiResponse
import com.onrender.maskedredstonerproz.routes.*
import com.onrender.maskedredstonerproz.services.AnswerService
import com.onrender.maskedredstonerproz.services.QuestionService
import com.onrender.maskedredstonerproz.services.QuizService
import com.onrender.maskedredstonerproz.services.UserService
import com.onrender.maskedredstonerproz.util.Constants
import com.onrender.maskedredstonerproz.util.UserRole
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import org.koin.ktor.ext.inject

fun Application.configureRouting() {

    val userService: UserService by inject()
    val answerService: AnswerService by inject()
    val questionService: QuestionService by inject()
    val quizService: QuizService by inject()

    routing {
        get("/") {
            call.respondText("Hello World!")
        }

        // user routes
        signUp(userService)
        logIn(
            userService,
            System.getenv("DOMAIN")?.toString()
                ?: "localhost:8080",
            "com.onrender.maskedredstonerproz.trivia-trove",
            System.getenv("SECRET")?.toString()
                ?: "u/P;Am5e?=JU~,L)xVb_9s"
        )
        getLoggedInUser(userService)
        logOut()
        addCreatedQuiz(userService)
        addAssignedQuiz(userService)

        // answer routes
        createAnswer(answerService)
        checkAnswer(answerService)

        // question routes
        createQuestion(questionService)

        // quiz routes
        createQuiz(quizService)

        authenticate("web") {
            get("/api/data/getAllForType") {

                val dataId = call.parameters[Constants.QueryParams.DATA_ID] ?: run {
                    call.respond(
                        HttpStatusCode.BadRequest,
                        BasicApiResponse<Unit>(
                            successful = false
                        )
                    )
                    return@get
                }

                val dataType = call.parameters[Constants.QueryParams.DATA_TYPE] ?: run {
                    call.respond(
                        HttpStatusCode.BadRequest,
                        BasicApiResponse<Unit>(
                            successful = false
                        )
                    )
                    return@get
                }

                when (dataType) {

                    "quiz" -> {

                        val user = userService.getUserById(dataId) ?: run {
                            call.respond(
                                HttpStatusCode.ExpectationFailed,
                                BasicApiResponse<Unit>(
                                    successful = false,
                                    message = "There is no user with this id!"
                                )
                            )
                            return@get
                        }

                        call.respond(
                            HttpStatusCode.OK,
                            BasicApiResponse(
                                successful = true,
                                data = if (user.role == UserRole.PROFESSOR) {
                                    quizService.getQuizzesByIds(*user.createdQuizzes?.toTypedArray()!!)
                                } else quizService.getQuizzesByIds(*user.assignedQuizzes?.toTypedArray()!!)
                            )
                        )
                    }

                    "question" -> {

                        val quiz = quizService.getQuizzesByIds(dataId)[0] ?: run {
                            call.respond(
                                HttpStatusCode.ExpectationFailed,
                                BasicApiResponse<Unit>(
                                    successful = false,
                                    message = "There is no quiz with this id!"
                                )
                            )
                            return@get
                        }

                        call.respond(
                            HttpStatusCode.OK,
                            BasicApiResponse(
                                successful = true,
                                data = questionService.getQuestionsByIds(*quiz.questionIds.toTypedArray())
                            )
                        )

                    }

                    "answer" -> {

                        val question = questionService.getQuestionsByIds(dataId)[0] ?: run {
                            call.respond(
                                HttpStatusCode.ExpectationFailed,
                                BasicApiResponse<Unit>(
                                    successful = false,
                                    message = "There is no question with this id!"
                                )
                            )
                            return@get
                        }

                        val answer = answerService.getAnswersByIds(question.answerId)[0]

                        when(answer) {
                            is Answer.FillTheBlanks -> {
                                call.respond(
                                    HttpStatusCode.OK,
                                    BasicApiResponse(
                                        successful = true,
                                        data = answer
                                    )
                                )
                                return@get
                            }
                            is Answer.MatchDescriptions -> {
                                call.respond(
                                    HttpStatusCode.OK,
                                    BasicApiResponse(
                                        successful = true,
                                        data = answer
                                    )
                                )
                                return@get
                            }
                            is Answer.MultipleChoice -> {
                                call.respond(
                                    HttpStatusCode.OK,
                                    BasicApiResponse(
                                        successful = true,
                                        data = answer
                                    )
                                )
                                return@get
                            }
                            is Answer.Sequencing -> {
                                call.respond(
                                    HttpStatusCode.OK,
                                    BasicApiResponse(
                                        successful = true,
                                        data = answer
                                    )
                                )
                                return@get
                            }
                            is Answer.SingleChoice -> {
                                call.respond(
                                    HttpStatusCode.OK,
                                    BasicApiResponse(
                                        successful = true,
                                        data = answer
                                    )
                                )
                                return@get
                            }
                            is Answer.TrueFalse -> {
                                call.respond(
                                    HttpStatusCode.OK,
                                    BasicApiResponse(
                                        successful = true,
                                        data = answer
                                    )
                                )
                                return@get
                            }
                            else -> Unit
                        }

                    }

                    else -> Unit

                }
            }
        }
    }
}