package com.onrender.maskedredstonerproz.plugins

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.auth.jwt.*
import io.ktor.server.response.*
import io.ktor.server.sessions.*
import java.io.File

data class UserSession(val id: String, val name: String): Principal

fun Application.configureSecurity() {
    authentication {
        jwt("android") {
            val jwtAudience = "com.onrender.maskedredstonerproz.trivia-trove"
            realm = "Access to the whole API"
            verifier(
                JWT
                    .require(Algorithm.HMAC256(System.getenv("SECRET")?.toString() ?: "u/P;Am5e?=JU~,L)xVb_9s"))
                    .withAudience(jwtAudience)
                    .withIssuer(System.getenv("DOMAIN")?.toString()?: "localhost:8080")
                    .build()
            )
            validate { credential ->
                if (credential.payload.audience.contains(jwtAudience)) JWTPrincipal(credential.payload) else null
            }
            skipWhen { call -> call.sessions.get<UserSession>() != null }
        }
        session<UserSession>("web") {
            validate { session ->
                if(session.name.startsWith("TRIVIATROVE")) {
                    session
                } else {
                    null
                }
            }
            challenge {
                call.respond(HttpStatusCode.Unauthorized)
            }
        }
    }
    install(Sessions) {
        cookie<UserSession>("USER_SESSION", directorySessionStorage(File("build/.sessions/"))) {
            cookie.secure = false
            cookie.maxAgeInSeconds = 60 * 60 * 1000
        }
    }
}