package com.onrender.maskedredstonerproz.plugins

import io.ktor.server.application.*
import org.koin.core.module.Module
import org.koin.ktor.plugin.Koin

fun Application.configureDI(module: Module) {
    install(Koin) {
        modules(module)
    }
}