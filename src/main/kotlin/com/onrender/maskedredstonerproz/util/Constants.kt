package com.onrender.maskedredstonerproz.util

object Constants {

    const val DATABASE_NAME = "trivia_trove_db"

    const val MINIMUM_PASSWORD_LENGTH = 16

    object QueryParams {

        const val DATA_ID = "data_id"

        const val DATA_TYPE = "data_type"
    }

}