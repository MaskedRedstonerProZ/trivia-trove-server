package com.onrender.maskedredstonerproz.util

import com.onrender.maskedredstonerproz.data.models.Answer
import com.onrender.maskedredstonerproz.data.requests.CheckAnswerRequest

fun CheckAnswerRequest.asAnswer(): Answer {
    return when(this) {
        is CheckAnswerRequest.FillTheBlanks -> {
            Answer.FillTheBlanks(
                sentences = sentences,
                positionsOfBlanks = positionsOfBlanks
            )
        }
        is CheckAnswerRequest.MatchDescriptions -> {
            Answer.MatchDescriptions(
                phrases = phrases,
                descriptions = descriptions
            )
        }
        is CheckAnswerRequest.MultipleChoice -> {
            Answer.MultipleChoice(
                choices = choices,
                correctAnswerChoices = correctAnswerChoices
            )
        }
        is CheckAnswerRequest.Sequencing -> {
            Answer.Sequencing(
                answerTextsInCorrectSequence = answerTextsInCorrectSequence
            )
        }
        is CheckAnswerRequest.SingleChoice -> {
            Answer.SingleChoice(
                choices = choices,
                correctAnswerChoice = correctAnswerChoice
            )
        }
        is CheckAnswerRequest.TrueFalse -> {
            Answer.TrueFalse(
                isTrue = isTrue
            )
        }
    }
}