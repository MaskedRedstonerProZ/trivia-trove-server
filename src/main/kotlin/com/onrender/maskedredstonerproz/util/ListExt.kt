package com.onrender.maskedredstonerproz.util

import com.onrender.maskedredstonerproz.data.models.BlankPosition

fun List<String>.makePositionsOfBlanks(vararg blankFillers: String): List<BlankPosition> {
    val list = mutableListOf<BlankPosition>()

    this.forEachIndexed { index, s ->
        list.add(BlankPosition(s.indexOf(blankFillers[index]), s.indexOf(blankFillers[index]) + blankFillers[index].lastIndex))
    }

    return list
}