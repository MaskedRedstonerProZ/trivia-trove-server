package com.onrender.maskedredstonerproz.util.serializer

import com.onrender.maskedredstonerproz.data.requests.CreateAnswerRequest
import kotlinx.serialization.DeserializationStrategy
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.jsonObject
import kotlinx.serialization.json.jsonPrimitive

object CreateAnswerRequestSerializer : JsonContentPolymorphicSerializer<CreateAnswerRequest>(CreateAnswerRequest::class) {

    override fun selectDeserializer(element: JsonElement): DeserializationStrategy<CreateAnswerRequest> =
        when (element.jsonObject["type"]?.jsonPrimitive?.content) {
            "sc" -> CreateAnswerRequest.SingleChoice.serializer()
            "mc" -> CreateAnswerRequest.MultipleChoice.serializer()
            "tf" -> CreateAnswerRequest.TrueFalse.serializer()
            "fb" -> CreateAnswerRequest.FillTheBlanks.serializer()
            "md" -> CreateAnswerRequest.MatchDescriptions.serializer()
            "sqc" -> CreateAnswerRequest.Sequencing.serializer()
            else -> throw Exception("Unknown Answer: key 'type' not found or does not matches any module type")
        }

}