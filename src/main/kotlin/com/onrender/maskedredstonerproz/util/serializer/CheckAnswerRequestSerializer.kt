package com.onrender.maskedredstonerproz.util.serializer

import com.onrender.maskedredstonerproz.data.requests.CheckAnswerRequest
import kotlinx.serialization.DeserializationStrategy
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.jsonObject
import kotlinx.serialization.json.jsonPrimitive

object CheckAnswerRequestSerializer : JsonContentPolymorphicSerializer<CheckAnswerRequest>(CheckAnswerRequest::class) {

    override fun selectDeserializer(element: JsonElement): DeserializationStrategy<CheckAnswerRequest> =
        when (element.jsonObject["type"]?.jsonPrimitive?.content) {
            "sc" -> CheckAnswerRequest.SingleChoice.serializer()
            "mc" -> CheckAnswerRequest.MultipleChoice.serializer()
            "tf" -> CheckAnswerRequest.TrueFalse.serializer()
            "fb" -> CheckAnswerRequest.FillTheBlanks.serializer()
            "md" -> CheckAnswerRequest.MatchDescriptions.serializer()
            "sqc" -> CheckAnswerRequest.Sequencing.serializer()
            else -> throw Exception("Unknown CheckAnswerRequest: key 'type' not found or does not matches any module type")
        }

}
