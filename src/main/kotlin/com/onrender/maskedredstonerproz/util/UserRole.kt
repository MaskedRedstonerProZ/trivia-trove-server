package com.onrender.maskedredstonerproz.util

object UserRole {

    const val PROFESSOR = 0

    const val STUDENT = 1

}