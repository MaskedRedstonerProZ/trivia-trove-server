package com.onrender.maskedredstonerproz.di

import com.onrender.maskedredstonerproz.data.repos.*
import com.onrender.maskedredstonerproz.services.AnswerService
import com.onrender.maskedredstonerproz.services.QuestionService
import com.onrender.maskedredstonerproz.services.QuizService
import com.onrender.maskedredstonerproz.services.UserService
import com.onrender.maskedredstonerproz.util.Constants
import org.koin.dsl.module
import org.litote.kmongo.coroutine.coroutine
import org.litote.kmongo.reactivestreams.KMongo

val mainModule = module {

    single {
        val url = System.getenv("DATABASE_URL") ?: "mongodb://localhost:27017"
        val client = KMongo.createClient(url).coroutine
        client.getDatabase(Constants.DATABASE_NAME)
    }

    single<UserRepository> {
        UserRepositoryImpl(get())
    }

    single<AnswerRepository> {
        AnswerRepositoryImpl(get())
    }

    single<QuestionRepository> {
        QuestionRepositoryImpl(get())
    }

    single<QuizRepository> {
        QuizRepositoryImpl(get())
    }

    single {
        UserService(get())
    }

    single {
        AnswerService(get())
    }

    single {
        QuestionService(get())
    }

    single {
        QuizService(get())
    }

}