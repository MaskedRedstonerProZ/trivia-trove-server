package com.onrender.maskedredstonerproz.routes

import com.onrender.maskedredstonerproz.data.models.Question
import com.onrender.maskedredstonerproz.data.requests.CreateQuestionRequest
import com.onrender.maskedredstonerproz.data.responses.BasicApiResponse
import com.onrender.maskedredstonerproz.data.responses.QuestionResponse
import com.onrender.maskedredstonerproz.services.QuestionService
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.createQuestion(service: QuestionService) {
    authenticate("android", "web") {
        post("/api/question/create") {
            val request = call.receiveNullable<CreateQuestionRequest>() ?: run {
                call.respond(HttpStatusCode.BadRequest)
                return@post
            }

            val question = Question(
                text = request.text,
                answerId = request.answerId
            )

            service.createQuestion(question)

            call.respond(
                HttpStatusCode.OK,
                BasicApiResponse(
                    successful = true,
                    data = QuestionResponse(
                        question.id
                    )
                )
            )
        }
    }
}