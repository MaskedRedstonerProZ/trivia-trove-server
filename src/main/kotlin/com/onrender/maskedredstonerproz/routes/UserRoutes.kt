package com.onrender.maskedredstonerproz.routes

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.onrender.maskedredstonerproz.data.models.User
import com.onrender.maskedredstonerproz.data.requests.UserAddAssignedQuizRequest
import com.onrender.maskedredstonerproz.data.requests.UserAddCreatedQuizRequest
import com.onrender.maskedredstonerproz.data.requests.UserLoginRequest
import com.onrender.maskedredstonerproz.data.requests.UserSignUpRequest
import com.onrender.maskedredstonerproz.data.responses.BasicApiResponse
import com.onrender.maskedredstonerproz.data.responses.LoginResponse
import com.onrender.maskedredstonerproz.plugins.UserSession
import com.onrender.maskedredstonerproz.services.UserService
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.server.sessions.*
import java.util.*

fun Route.signUp(service: UserService) {
    post("/api/user/signUp") {

        val request = call.receiveNullable<UserSignUpRequest>() ?: run {
            call.respond(HttpStatusCode.BadRequest)
            return@post
        }

        if (service.doesUserWithEmailExist(request.email)) {
            call.respond(
                HttpStatusCode.ExpectationFailed,
                BasicApiResponse<Unit>(
                    successful = false,
                    message = "A user with this email already exists!"
                )
            )
            return@post
        }

        try {
            service.signUp(
                User(
                    name = request.name,
                    role = request.role,
                    email = request.email,
                    password = request.password
                )
            )

            call.respond(HttpStatusCode.OK, BasicApiResponse<Unit>(true))
        } catch (e: Exception) {
            call.respond(
                HttpStatusCode.ExpectationFailed,
                BasicApiResponse<Unit>(successful = false, message = e.message)
            )
        }
    }
}

fun Route.logIn(
    service: UserService,
    jwtIssuer: String,
    jwtAudience: String,
    jwtSecret: String
) {
    post("/api/user/logIn") {

        val request = call.receiveNullable<UserLoginRequest>() ?: run {
            call.respond(HttpStatusCode.BadRequest)
            return@post
        }

        val user = service.getUserByEmail(request.email) ?: run {
            call.respond(
                HttpStatusCode.OK,
                BasicApiResponse<Unit>(
                    successful = false,
                    message = "There is no user with that email!"
                )
            )
            return@post
        }

        if(service.isPasswordValid(request.password, user.password)) {
            when(request.platformOfOrigin) {

                "Web" -> {
                    call.sessions.set(UserSession(user.id, "TRIVIATROVE-${user.name}"))
                    call.respond(
                        HttpStatusCode.OK,
                        BasicApiResponse(
                            successful = true,
                            data = LoginResponse.WebLoginResponse(user)
                        )
                    )
                }

                "Android" -> {
                    val expiresIn = 1000L * 60L * 60L * 24L * 365L
                    val token = JWT.create()
                        .withClaim("userId", user.id)
                        .withIssuer(jwtIssuer)
                        .withExpiresAt(Date(System.currentTimeMillis() + expiresIn))
                        .withAudience(jwtAudience)
                        .sign(Algorithm.HMAC256(jwtSecret))

                    call.respond(
                        HttpStatusCode.OK,
                        BasicApiResponse(
                            successful = true,
                            data = LoginResponse.AndroidLoginResponse(
                                id = user.id,
                                token = token
                            )
                        )
                    )
                }

            }
        }

    }
}

fun Route.getLoggedInUser(service: UserService) {
    authenticate("web") {
        get("/api/user/getAuthenticated") {

            call.respond(
                HttpStatusCode.OK,
                BasicApiResponse(
                    successful = true,
                    data = LoginResponse.WebLoginResponse(service.getUserById(call.sessions.get(UserSession::class)?.id ?: return@get) ?: run {
                        call.respond(HttpStatusCode.BadRequest)
                        return@get
                    })
                )
            )

        }
    }
}

fun Route.logOut() {
    authenticate("web") {
        delete("/api/user/logOut") {
            call.sessions.clear<UserSession>()
            call.respond(HttpStatusCode.OK)
        }
    }
}

fun Route.addCreatedQuiz(service: UserService) {
    authenticate("android", "web") {
        post("/api/user/createdQuiz/add") {
            val request = call.receiveNullable<UserAddCreatedQuizRequest>() ?: run {
                call.respond(HttpStatusCode.BadRequest)
                return@post
            }

            try {
                service.updateUser(request.email, listOf(request.quizId))

                call.respond(
                    HttpStatusCode.OK,
                    BasicApiResponse<Unit>(
                        successful = true
                    )
                )
            } catch (e: Exception) {
                call.respond(
                    HttpStatusCode.ExpectationFailed,
                    BasicApiResponse<Unit>(successful = false, message = e.message)
                )
            }
        }
    }
}

fun Route.addAssignedQuiz(service: UserService) {
    authenticate("android", "web") {
        post("/api/user/assignedQuiz/add") {
            val request = call.receiveNullable<UserAddAssignedQuizRequest>() ?: run {
                call.respond(HttpStatusCode.BadRequest)
                return@post
            }

            try {
                service.updateUser(request.email, assignedQuizzes = listOf(request.quizId))

                call.respond(
                    HttpStatusCode.OK,
                    BasicApiResponse<Unit>(
                        successful = true
                    )
                )
            } catch (e: Exception) {
                call.respond(
                    HttpStatusCode.ExpectationFailed,
                    BasicApiResponse<Unit>(successful = false, message = e.message)
                )
            }
        }
    }
}