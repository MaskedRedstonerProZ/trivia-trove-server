package com.onrender.maskedredstonerproz.routes

import com.onrender.maskedredstonerproz.data.models.Answer
import com.onrender.maskedredstonerproz.data.requests.CheckAnswerRequest
import com.onrender.maskedredstonerproz.data.requests.CreateAnswerRequest
import com.onrender.maskedredstonerproz.data.responses.AnswerResponse
import com.onrender.maskedredstonerproz.data.responses.BasicApiResponse
import com.onrender.maskedredstonerproz.data.responses.CheckAnswerResponse
import com.onrender.maskedredstonerproz.services.AnswerService
import com.onrender.maskedredstonerproz.util.asAnswer
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.createAnswer(service: AnswerService) {
    authenticate("android", "web") {
        post("/api/answer/create") {
            val request = call.receiveNullable<CreateAnswerRequest>() ?: run {
                call.respond(HttpStatusCode.BadRequest)
                return@post
            }

            val answer: Answer

            when (request) {

                is CreateAnswerRequest.FillTheBlanks -> {
                    answer = Answer.FillTheBlanks(
                        sentences = request.sentences,
                        positionsOfBlanks = request.positionsOfBlanks
                    )
                }

                is CreateAnswerRequest.MatchDescriptions -> {
                    answer = Answer.MatchDescriptions(
                        phrases = request.phrases,
                        descriptions = request.descriptions
                    )
                }

                is CreateAnswerRequest.MultipleChoice -> {
                    answer = Answer.MultipleChoice(
                        choices = request.choices,
                        correctAnswerChoices = request.correctAnswerChoices
                    )
                }

                is CreateAnswerRequest.Sequencing -> {
                    answer = Answer.Sequencing(
                        answerTextsInCorrectSequence = request.answerTextsInCorrectSequence
                    )
                }

                is CreateAnswerRequest.SingleChoice -> {
                    answer = Answer.SingleChoice(
                        choices = request.choices,
                        correctAnswerChoice = request.correctAnswerChoice
                    )
                }

                is CreateAnswerRequest.TrueFalse -> {
                    answer = Answer.TrueFalse(
                        isTrue = request.isTrue
                    )
                }
            }

            service.createAnswer(answer)

            call.respond(
                HttpStatusCode.OK,
                BasicApiResponse(
                    successful = true,
                    data = AnswerResponse(
                        answer.id
                    )
                )
            )
        }
    }
}

fun Route.checkAnswer(service: AnswerService) {
    authenticate("android", "web") {
        post("/api/answer/check") {

            val request = call.receiveNullable<CheckAnswerRequest>() ?: run {
                call.respond(HttpStatusCode.BadRequest)
                return@post
            }

            val rightAnswer = service.getAnswersByIds(request.answerId)[0]

            val isCorrect = when(request) {
                is CheckAnswerRequest.FillTheBlanks -> {
                    request.asAnswer() == rightAnswer
                }
                is CheckAnswerRequest.MatchDescriptions -> {
                    request.asAnswer() == rightAnswer
                }
                is CheckAnswerRequest.MultipleChoice -> {
                    request.asAnswer() == rightAnswer
                }
                is CheckAnswerRequest.Sequencing -> {
                    request.asAnswer() == rightAnswer
                }
                is CheckAnswerRequest.SingleChoice -> {
                    request.asAnswer() == rightAnswer
                }
                is CheckAnswerRequest.TrueFalse -> {
                    request.asAnswer() == rightAnswer
                }
            }

            call.respond(
                HttpStatusCode.OK,
                BasicApiResponse(
                    successful = true,
                    data = CheckAnswerResponse(
                        request.answerId,
                        isCorrect = isCorrect
                    )
                )
            )

        }
    }
}