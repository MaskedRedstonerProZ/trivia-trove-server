package com.onrender.maskedredstonerproz.routes

import com.onrender.maskedredstonerproz.data.models.Quiz
import com.onrender.maskedredstonerproz.data.requests.CreateQuizRequest
import com.onrender.maskedredstonerproz.data.responses.BasicApiResponse
import com.onrender.maskedredstonerproz.data.responses.QuizResponse
import com.onrender.maskedredstonerproz.services.QuizService
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.createQuiz(service: QuizService) {
    authenticate("android", "web") {
        post("/api/quiz/create") {

            val request = call.receiveNullable<CreateQuizRequest>()?: run {
                call.respond(HttpStatusCode.BadRequest)
                return@post
            }
            
            val quiz = Quiz(
                title = request.title,
                description = request.description,
                questionIds = request.questionIds
            )

            service.createQuiz(quiz)

            call.respond(
                HttpStatusCode.OK,
                BasicApiResponse(
                    successful = true,
                    data = QuizResponse(
                        quiz.id
                    )
                )
            )

        }
    }
}