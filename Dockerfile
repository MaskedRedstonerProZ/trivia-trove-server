FROM amazoncorretto:19.0.1
EXPOSE 8080:8080
ENV DATABASE_URL=mongodb://mongo:27017
RUN mkdir /app && mkdir /app/project
COPY . /app/project
RUN cd /app/project && ./gradlew buildFatJar && cp ./build/libs/trivia-trove-server.jar ../trivia-trove-server.jar && rm -rf /app/project
ENTRYPOINT ["java","-jar","/app/trivia-trove-server.jar"]